import cv2
import time
import numpy as np
import math

folder = "dataset/images/testScreenNew/"
maskCar = cv2.imread('test/maskTM/mask3.png', 0)
l_h, l_s, l_v, u_h, u_s, u_v = 45, 0, 180, 255, 30, 255

frameI=0
pause = False

coefA = [10, 2, 1, 0.8, 0.6, 0.4, 0.2]
signes = [-1, 1]
x = 480
affineFunc = lambda a, x, b : round(a * x + b)

def findDistanceWall(contour):
    blank = np.zeros(image.shape)
    img1 = cv2.drawContours(blank.copy(), [contour], -1, 1, 2)

    imgVision = cv2.imread('test/maskTM/vision2.png')
    imgray = cv2.cvtColor(imgVision, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(imgray, 100, 255, cv2.THRESH_BINARY)
    contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    img2 = cv2.drawContours(blank.copy(), contours, -1, 1, 1)

    
    return intersection(img1, img2), img1, img2

def intersection(img1, img2):
    intersection =  cv2.bitwise_and(img1, img2)
    result = np.where(intersection == 255)
    # print(result)

    intersX = result[1]
    intersY = result[0]
    # distance = round(math.sqrt((480 - inter[0])**2 + (540 - inter[1])**2))
    inters = sorted(zip(intersX, intersY), key=lambda x: round(math.sqrt((480 - x[0])**2 + (540 - x[1])**2)), reverse=True)
    # print(len(intersX))
    result = []
    leadCoefs = []
    for inter in inters:
        find = False
        findCoef = False
        for res in result:
            if inter[0] - 20 <= res[0] <= inter[0] + 20 and inter[1] - 20 <= res[1] <= inter[1] + 20:
                find = True
        if not (470 <= inter[0] <= 490 and 530 <= inter[1] <= 550):
            if not find:
                leadCoef = ((560 - inter[1])/(480 - inter[0]))
                # print(leadCoef)
                for leadCoefOld in leadCoefs:
                    if leadCoef - 0.2 <= leadCoefOld <= leadCoef + 0.2:
                        findCoef = True
                if not findCoef:
                    leadCoefs.append(leadCoef)
                    result.append(inter)

    result = sorted(result, key=lambda x: x[0])

    print(len(result))
    # print(leadCoefs)

    return result

while True:
    ts = time.time()
    tickmark=cv2.getTickCount()
    orig = cv2.imread(folder + 'test' + str(frameI) + '.png')
    frame = orig.copy()

    lo = np.array([l_h, l_s, l_v])
    hi = np.array([u_h, u_s, u_v])

    image=cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    image=cv2.blur(image, (8, 8))
    mask=cv2.inRange(image, lo, hi)
    mask=cv2.erode(mask, None, iterations=2)
    mask=cv2.dilate(mask, None, iterations=2)

    mask = cv2.bitwise_not(mask)
    mask=cv2.bitwise_and(mask, mask, mask=maskCar)
    mask = cv2.bitwise_not(mask)
    
    elements=cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    distances= []
    line = np.zeros(orig.shape)
    contour = np.zeros(orig.shape)
    if elements :
        c=max(elements, key=cv2.contourArea)
        cv2.drawContours(contour, [c], -1, (255, 255, 255), 1)


    for signe in signes:
        for a in coefA:
            x = x*signe
            y = affineFunc(a, x, 0)*signe
            line = cv2.line(line , (x+480, 560-y), (480, 540), (255, 255, 255), 1)

    inters = intersection(contour, line)

    for i, inter in enumerate(inters):
                imgWithLine = cv2.line(frame , (inter[0], inter[1]), (480, 540), (0, 255, 0), 2)
                cv2.circle(imgWithLine, (inter[0], inter[1]), 2, (0,0,255), -1)
                cv2.putText(imgWithLine, str(i), (inter[0]-20, inter[1]), cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                distance = round(math.sqrt((480 - inter[0])**2 + (540 - inter[1])**2))
                cv2.putText(imgWithLine, str(distance), (inter[0]-20, inter[1]-20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                distances.append(distance)

    fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
    cv2.putText(orig, "FPS: {:05.2f}".format(fps), (10, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)
    cv2.imshow('video', orig)
    cv2.imshow('contour', contour)
    cv2.imshow('line', imgWithLine)
    # cv2.imshow('res', res)


    key=cv2.waitKey(1)
    if key==ord('q'):
        break
    if key==ord('p'):
        pause = pause == False

    if not pause:
        frameI += 2
        if frameI > 476:
            frameI = 0
    # print("total : ", time.time() - ts)
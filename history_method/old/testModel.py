from random import shuffle
import cv2
import numpy as np

from tensorflow import keras

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '0' 
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed
# 3 = INFO, WARNING, and ERROR messages are not printed

class TmModel(keras.models.Sequential):

    def __init__(self):
        super(TmModel, self).__init__()
        # Processing image
        self.ld = keras.layers.Lambda(lambda x: (x / 127) - 1, input_shape = (540, 960, 3))
        self.cropping = keras.layers.Cropping2D(cropping=((300, 100), (0, 0)), input_shape = (540, 960, 3))
        # Convolutions
        self.conv1 = keras.layers.Conv2D(8, 9, strides=(4, 4), padding="same", activation="elu")
        self.conv2 = keras.layers.Conv2D(16, 5, strides=(2, 2), padding="same", activation="elu")
        self.conv3 = keras.layers.Conv2D(32, 4, strides=(1, 1), padding="same", activation="elu")
        # Flatten the convolution
        self.flatten = keras.layers.Flatten()       
        # Dense and Dropout layers
        self.drop1 = keras.layers.Dropout(.5)
        self.d1 = keras.layers.Dense(1024, activation='elu')
        self.drop2 = keras.layers.Dropout(.3)
        self.out = keras.layers.Dense(3)
        self.compile(loss="mse", optimizer="adam")

    def call(self, image):
        ld = self.ld(image)
        cropp = self.cropping(ld)
        conv1 = self.conv1(cropp)
        conv2 = self.conv2(conv1)
        conv3 = self.conv3(conv2)
        flatten = self.flatten(conv3)
        drop1 = self.drop1(flatten)
        d1 = self.d1(drop1)
        drop2 = self.drop2(d1)
        output = self.out(drop2)
        return output

    def get_data(self, log_content, index_list, batch_size, strict=True):
        images, controlls = [], []
        while True:
            if not strict:
                shuffle(index_list)
            for index in index_list:
                # Get the Image
                img = cv2.imread(log_content[index]["imgPath"])
                if img is None: continue
                # Get the Controll
                controll = log_content[index]["target"]
                # Append the new image/controll to the current batch
                images.append(img)
                controlls.append(controll)
                # Yield the new batch
                if len(images) >= batch_size:
                    yield np.array(images), np.array(controlls), [None]
                    images, controlls = [], []
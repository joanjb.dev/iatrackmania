import cv2
import numpy as np
import time
import math
import threading
import os

folder = "dataset/images/testScreenNew/"
# folder = "dataset/images/circuit2/"
maskCar = cv2.imread('test/maskTM/mask3.png', 0)

nbFile = len(os.listdir(folder))

trakingBar = False
# l_h, l_s, l_v, u_h, u_s, u_v = 45, 0, 180, 255, 30, 255
# l_h, l_s, l_v, u_h, u_s, u_v = 0, 0, 180, 255, 255, 255
l_h, l_s, l_v, u_h, u_s, u_v = 0, 0, 180, 255, 90, 250

coefA = [10, 5, 2, 1.5, 1, 0.8, 0.6]
signes = [-1, 1]
xLine = 480
affineFunc = lambda a, x, b : round(a * x + b)

distanceTmp = [0] * (len(coefA)*len(signes))

frameI=0
pause = False

def intersectionWithFunc(img1, img2, addX, index):
    global distanceTmp
    intersection =  cv2.bitwise_and(img1, img2) # <- need opti
    result = np.where(intersection == 255)
    
    intersX = result[1] + addX
    intersY = result[0]

    inters = sorted(zip(intersX, intersY), key=lambda x: round(math.sqrt((480 - x[0])**2 + (540 - x[1])**2)), reverse=True)        

    distanceTmp[index] = inters[0]

def findDistanceWall(c):
    global distanceTmp
    contour = np.zeros(orig.shape)

    cv2.drawContours(contour, [c], -1, (255, 255, 255), 1)
    cN = contour[0:540, 0:480]
    cP = contour[0:540, 480:960]

    threads = []
    i = 0

    for signe in signes:
        for a in coefA:
            addX = 0
            x = xLine*signe
            y = affineFunc(a, x, 0)*signe

            line = np.zeros(orig.shape)
            line = cv2.line(line , (x+480, 540-y), (480, 540), (255, 255, 255), 2)
            if x < 0:
                cUse = cN
                line = line[0:540, 0:480]
            else:
                cUse = cP
                line = line[0:540, 480:960]
                addX = 480

            # img1 = img1[0:540, 480:960]
            thread = threading.Thread(target=intersectionWithFunc, args=(cUse, line, addX, i,))
            threads.append(thread)
            thread.start()
            i += 1

    for thread in threads:
        thread.join()
    
    distanceRes = sorted(distanceTmp, key=lambda x: x[0])

    return distanceRes, contour, line


def initWindowsBar():
    cv2.namedWindow("Tracking")
    cv2.resizeWindow("Tracking", 600,300)
    cv2.createTrackbar("LH", "Tracking", 0, 255, lambda x : None)
    cv2.createTrackbar("LS", "Tracking", 0, 255, lambda x : None)
    cv2.createTrackbar("LV", "Tracking", 0, 255, lambda x : None)
    cv2.createTrackbar("UH", "Tracking", 255, 255, lambda x : None)
    cv2.createTrackbar("US", "Tracking", 255, 255, lambda x : None)
    cv2.createTrackbar("UV", "Tracking", 255, 255, lambda x : None)

def displayWindowsBar():
    global l_h, l_s, l_v, u_h, u_s, u_v
    l_h = cv2.getTrackbarPos("LH", "Tracking")
    l_s = cv2.getTrackbarPos("LS", "Tracking")
    l_v = cv2.getTrackbarPos("LV", "Tracking")

    u_h = cv2.getTrackbarPos("UH", "Tracking")
    u_s = cv2.getTrackbarPos("US", "Tracking")
    u_v = cv2.getTrackbarPos("UV", "Tracking")

if trakingBar:
    initWindowsBar()

while True:
    ts = time.time()
    tickmark=cv2.getTickCount()
    orig = cv2.imread(folder + 'test' + str(frameI) + '.png')
    frame = orig.copy()

    if not pause :
        if trakingBar:
            displayWindowsBar()

        lo = np.array([l_h, l_s, l_v])
        hi = np.array([u_h, u_s, u_v])

        image=cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        image=cv2.blur(image, (8, 8))
        mask=cv2.inRange(image, lo, hi)
        mask=cv2.erode(mask, None, iterations=2)
        mask=cv2.dilate(mask, None, iterations=2)

        mask = cv2.bitwise_not(mask)
        mask=cv2.bitwise_and(mask, mask, mask=maskCar)
        mask = cv2.bitwise_not(mask)
        
        elements=cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        distances= []
        if elements :
            c=max(elements, key=cv2.contourArea)
            inters, img1, img2 = findDistanceWall(c)
            for i, inter in enumerate(inters):
                imgWithLine = cv2.line(frame , (inter[0], inter[1]), (480, 540), (0, 255, 0), 2)
                cv2.circle(imgWithLine, (inter[0], inter[1]), 2, (0,0,255), -1)
                cv2.putText(imgWithLine, str(i), (inter[0]-20, inter[1]), cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                distance = round(math.sqrt((480 - inter[0])**2 + (540 - inter[1])**2))
                cv2.putText(imgWithLine, str(distance), (inter[0]-20, inter[1]-20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                distances.append(distance)

        print(len(distances), distances)

        
        
        fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
        cv2.putText(orig, "FPS: {:05.2f}".format(fps), (10, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)
        cv2.imshow('video', orig)
        cv2.imshow('mask', mask)
        # cv2.imshow('contour', img1)
        cv2.imshow('line', imgWithLine)


    key=cv2.waitKey(1)
    if key==ord('q'):
        break
    if key==ord('p'):
        pause = pause == False

    if not pause:
        frameI += 2
        if frameI > nbFile - 2:
            frameI = 0
    print("timeDistance : ", time.time() - ts)

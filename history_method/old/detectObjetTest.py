import cv2
import numpy as np
import time

def getImgWithContour(image, imageAdd=None):
    imgray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(imgray, 127, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    # cv2.drawContours(image, contours, -1, (0,255,0), 3)
    # -----------
    # gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # gray = cv2.bilateralFilter(gray, 11, 17, 17)
    # edged = cv2.Canny(gray, 30, 200)
    # cnts1 = cv2.findContours(edged.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # cnts2 = imutils.grab_contours(cnts1)
    # cnts = sorted(cnts2, key = cv2.contourArea, reverse = True)[:10]
    # # screenCnt = None
    # # loop over our contours
    maxContour = None
    maxPeri = 0
    for c in contours:
        # approximate the contour
        peri = cv2.arcLength(c, True)

        if peri > maxPeri:
            maxContour = c
            maxPeri = peri

        # approx = cv2.approxPolyDP(c, 0.015 * peri, True)
        # if our approximated contour has four points, then
        # we can assume that we have found our screen
        # if peri >= 200:

        # cv2.drawContours(image, [c], -1, (0, 0, 255), 2)
        # if imageAdd is not None:
        #     cv2.drawContours(imageAdd, [c], -1, (0, 0, 255), cv2.LINE_4)

        # if len(approx) == 4:
        #     screenCnt = approx
        #     break
    
    if maxPeri > 0:
        cv2.drawContours(image, [maxContour], -1, (0, 0, 255), 2)

    return image, thresh, imageAdd, maxContour
            
def getMaskImage(image, trackBar=False, addMask=None, l_b=None, u_b = None, init=False):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    if not init:
        if trackBar:
            cv2.namedWindow("Tracking")
            cv2.resizeWindow("Tracking", 600,300)
            cv2.createTrackbar("LH", "Tracking", 0, 255, lambda x : None)
            cv2.createTrackbar("LS", "Tracking", 0, 255, lambda x : None)
            cv2.createTrackbar("LV", "Tracking", 0, 255, lambda x : None)
            cv2.createTrackbar("UH", "Tracking", 255, 255, lambda x : None)
            cv2.createTrackbar("US", "Tracking", 255, 255, lambda x : None)
            cv2.createTrackbar("UV", "Tracking", 255, 255, lambda x : None)

    if trackBar:
        l_h = cv2.getTrackbarPos("LH", "Tracking")
        l_s = cv2.getTrackbarPos("LS", "Tracking")
        l_v = cv2.getTrackbarPos("LV", "Tracking")

        u_h = cv2.getTrackbarPos("UH", "Tracking")
        u_s = cv2.getTrackbarPos("US", "Tracking")
        u_v = cv2.getTrackbarPos("UV", "Tracking")

        l_b = [l_h, l_s, l_v]
        u_b = [u_h, u_s, u_v]
    
    l_b = np.array(l_b)
    u_b = np.array(u_b)

    mask = cv2.inRange(hsv, l_b, u_b)
    mask = 255 - mask

    if not addMask is None:
        #addMask = 255- addMask
        mask = cv2.bitwise_and(mask, mask, mask=addMask)

    res = cv2.bitwise_and(image, image, mask=mask)
    Imagenull = np.ones(image.shape) * 255
    mask = cv2.bitwise_and(Imagenull, Imagenull, mask=mask)
    mask = mask.astype('uint8')
    return res, mask

def morphology(image):
    kernel = np.ones((5, 5), np.uint8)

    image = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)
    image = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)

    return image

def conv2d(image):
    kernel = np.ones((8,8),np.float32)/25
    dst = cv2.filter2D(image,-1,kernel)

    return dst

def blur(image):
    blur = cv2.blur(image,(20,20))
    return blur

def gaussian(image):
    blur = cv2.GaussianBlur(image,(5,5),0)
    return blur


def median(image):
    median = cv2.medianBlur(image,5)
    return median


def bilateral(image):
    blur = cv2.bilateralFilter(image,9,75,75)
    return blur

# for HSV -----
maskIDE = cv2.imread('test/maskTM/mask.png', 0)
# l_tm = [0, 0, 40]
# u_tm = [255, 255, 255]
# l_tmn = [0, 0, 140]
# u_tmn = [255, 255, 255]
l_tmn = [0, 0, 0]
u_tmn = [0, 0, 255]
isInit = lambda x : x > 0
# -----

folder = "dataset/images/testScreenNEW/"

frameI=0
pause = False

th1 , th2 = 130, 255
while True:
    image = cv2.imread(folder + 'test' + str(frameI) + '.png')
    # image = cv2.imread('testScreenNEW/test267.png')
    orig = image.copy()
    tickmark=cv2.getTickCount()

    res = image.copy()
    imgWithLine = image.copy()

    imageMorph = morphology(image)
    imageConv = conv2d(imageMorph)
    imageBlur = cv2.blur(image, (3, 3))
    imageGauss = gaussian(image)
    imageMedian = median(image)
    imageB = bilateral(image)

    resMask, mask = getMaskImage(imageB, addMask=maskIDE, l_b=l_tmn, u_b=u_tmn, init=isInit(frameI), trackBar=True)
    # resMask, mask = getMaskImage(image, addMask=maskIDE, init=isInit(frameI), trackBar=True)
    oResMask, oMask = resMask.copy(), mask.copy()
    maskMorph = morphology(mask)
    maskMorphO = maskMorph.copy()

    maskMedian = median(maskMorph)



    gray = cv2.cvtColor(resMask, cv2.COLOR_BGR2GRAY)
    grayB = cv2.blur(gray, (3, 3))



    canny = cv2.Canny(grayB, th1, th2)


    imagectr, edged, res, maxContour = getImgWithContour(mask, res)

    blank = np.zeros(image.shape)
    img1 = cv2.drawContours(blank.copy(), [maxContour], -1, 1) 


    imgVision = cv2.imread('test/maskTM/vision2.png')
    imgray = cv2.cvtColor(imgVision, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(imgray, 100, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    img2 = cv2.drawContours(blank.copy(), contours, -1, 1)

    intersection = np.logical_and(img1, img2)
    intersection = intersection.astype(int)
    intersection = intersection*255
    result = np.where(intersection == 255)
    # print(result)

    intersX = result[1]
    intersY = result[0]

    tmpsXY = []
    index = 0
    for interX in intersX:
        find = False
        for tmpXY in tmpsXY:
            if tmpXY[0] - 5 < interX < tmpXY[0] + 5:
                if tmpXY[1] - 5 < intersY[index] < tmpXY[1] + 5:
                    find = True
        
        if not find:
            tmpsXY.append([intersX[index], intersY[index]])
            index += 1
        else:
            intersX = np.delete(intersX, index)
            intersY = np.delete(intersY, index)
    beginAt = len(intersX)//2
    intersX = intersX[beginAt:]
    intersY = intersY[beginAt:]

    # print(intersX, intersY)

    for i in range(len(intersX)):
        imgWithLine = cv2.line(imgWithLine , (intersX[i], intersY[i]), (480, 540), (0, 255, 0), 2) 

    fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
    cv2.putText(orig, "FPS: {:05.2f}".format(fps), (10, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)

    cv2.putText(orig, "[u|j]th1: {:d}  [i|k]th2: {:d}  [y|h]blur: {:d}".format(th1, th2, 3), (10, 40), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 255, 255), 1)
    cv2.imshow("orig", orig)
    cv2.imshow("mask", resMask)
    cv2.imshow("imagectr", imagectr)
    # cv2.imshow("res", res)
    cv2.imshow("line", imgWithLine)
    cv2.imshow("canny", canny)

    key=cv2.waitKey(1)
    if key==ord('q'):
        break
    if key==ord('p'):
        pause = pause == False

    frameI += 2
    if frameI > 418:
        frameI = 2

cv2.destroyAllWindows()
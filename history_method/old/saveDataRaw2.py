from pynput import keyboard
from PIL import ImageGrab
import time
import json
import cv2
import numpy as np
import os

#CONTROLL = [[0,0,0],[1,0,0],[1,1,0],[0,1,0],[1,0,1],[0,0,1],[1,1,1],[0,1,1]]

# no forward
CONTROLL = [[0,1],[1,0],[0,0]]

FILEDATASET = 'dataset/dsFull1.json'
FILECONFIG = 'dataset/confdFull1.json'
DIRIMAGES = './dataset/images/full1/'


if not os.path.exists(FILEDATASET):
    with open(FILEDATASET, 'w') as outfile:
        json.dump([], outfile)

if not os.path.exists(FILECONFIG):
    with open(FILECONFIG, 'w') as outfile:
        json.dump({"num": 0}, outfile)

if not os.path.isdir(DIRIMAGES):
    os.mkdir(DIRIMAGES)

continu = True

# The key combination to check
COMBINATION = {keyboard.Key.up, keyboard.Key.right, keyboard.Key.down, keyboard.Key.left}

# The currently active modifiers
current = set()
up = 0
right = 0
left = 0

def getInput(onehot):
    global up, right, left, CONTROLL
    if onehot:
        # inp = [0,0,0,0,0,0,0,0]
        if([right, left] != [1,1]):
            inp = [0,0,0]
            i = CONTROLL.index([right, left])
            #i = CONTROLL.index([up, right, left])
            inp[i] = 1
            return inp
        else:
            return None
    else:
        return [left, up, right]
        #return [up, right, left]

def on_press(key):
    global continu, up, right, left, current
    if key == keyboard.Key.up:
        up = 1
    if key == keyboard.Key.right:
        right = 1
    if key == keyboard.Key.left:
        left = 1
    if key in COMBINATION:
        current.add(key)
    if key == keyboard.Key.esc:
        listener.stop()
        continu = False
        return False


def on_release(key):
    global continu, up, right, left, current
    try:
        if key == keyboard.Key.up:
            up = 0
        if key == keyboard.Key.right:
            right = 0
        if key == keyboard.Key.left:
            left = 0
        current.remove(key)
    except KeyError:
        pass


listener = keyboard.Listener(
    on_press=on_press,
    on_release=on_release)
listener.start()

target = [0, 0, 0]
dataSet = []

data = []

# #percent by which the image is resized
scale_percent = 50

print('3s before starting')
time.sleep(3)
print('start')
while continu:
    start = time.time()
    
    inp = getInput(False)

    img = ImageGrab.grab()
    img = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2RGB)
    # #calculate the 50 percent of original dimensions
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    output = cv2.resize(img, (width, height))

    infos = {"img": output, "input" : inp}
    data.append(infos)
    time.sleep(0.01)
    os.system('cls')
    print('FPS : ', round(1. / (time.time() - start)))

print('end')

def getPathImg(i):
    return DIRIMAGES + 'test' + str(i) + '.png'

def flipData(info):
    imgFlip = cv2.flip(info['img'], 1)
    controllFlip = np.flip(info['input'], 0)
    return {"img": imgFlip, "input": controllFlip.tolist()}

with open(FILEDATASET) as json_file:
    dataSet = json.load(json_file)

with open(FILECONFIG) as json_file:
    config = json.load(json_file)

os.system('cls')
print('start saving images')
i=config["num"]
for infos in data:
    if i%100 == 0:
        os.system('cls')
        print('saving...')
        print("img num " + str(i))

    cv2.imwrite(getPathImg(i),  infos["img"])
    dataSet.append({"imgPath": getPathImg(i), "target" : infos["input"]})
    i += 1
    infosFlip = flipData(infos)
    cv2.imwrite(getPathImg(i), infosFlip["img"])
    dataSet.append({"imgPath": getPathImg(i), "target" : infosFlip["input"]})
    i += 1

    
os.system('cls')
print('end saving of ' + str(i) + ' images')

print('write json')
with open(FILEDATASET, 'w') as outfile:
    json.dump(dataSet, outfile)

with open(FILECONFIG, 'w') as outfile:
    json.dump({"num": i}, outfile)
print('finish')
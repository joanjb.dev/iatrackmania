## TEST MAIS UN PEU NUL, MAUVAISE SOLUTION POUR INPUT MANETTE

import json
import cv2
import os
import numpy as np
import random
from random import shuffle
from matplotlib import pyplot as plt

from sklearn.model_selection import train_test_split

import tensorflow as tf

from testModel import TmModel

DATA_PATH = "data/driving_log.csv"
DATA_IMG = "testScreen/"

def build_model():
    """
        Build keras model
    """
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Lambda(lambda x: (x / 127) - 1, input_shape = (540, 960, 3)))
    model.add(tf.keras.layers.Cropping2D(cropping=((245, 85), (0, 0)), input_shape = (540, 960, 3)))

    model.add(tf.keras.layers.Conv2D(96, 11, strides=(4, 4), padding="same", activation="elu"))
    model.add(tf.keras.layers.MaxPool2D(3, strides=2))
    model.add(tf.keras.layers.LayerNormalization())

    model.add(tf.keras.layers.Conv2D(256, 5, strides=(1, 1), padding="same", activation="elu"))
    model.add(tf.keras.layers.MaxPool2D(3, strides=2))
    model.add(tf.keras.layers.LayerNormalization())

    model.add(tf.keras.layers.Conv2D(256, 3, strides=(1, 1), padding="same", activation="elu"))
    model.add(tf.keras.layers.Conv2D(256, 3, strides=(1, 1), padding="same", activation="elu"))
    model.add(tf.keras.layers.Conv2D(256, 3, strides=(1, 1), padding="same", activation="elu"))
    model.add(tf.keras.layers.MaxPool2D(3, strides=2))
    model.add(tf.keras.layers.LayerNormalization())

    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(1024, activation="elu"))
    model.add(tf.keras.layers.Dropout(.5))
    model.add(tf.keras.layers.Dense(512, activation="elu"))
    model.add(tf.keras.layers.Dropout(.3))
    model.add(tf.keras.layers.Dense(3, activation='softmax'))

    #ada = optimizers.Adagrad(lr=0.001)
    model.compile(loss="mse", optimizer="adam")

    return model

def get_data(log_content, index_list, batch_size, strict=True):
    """
        Return Data from the simulator
        **input:
            *log_content (2dim Array) Data from the log file
            *index_list: Index to used to create each batch
            *batch_size (Int) Size of each batch
    """
    images, controlls = [], []
    while True:
        if not strict:
            shuffle(index_list)
        for index in index_list:
            img = cv2.imread(log_content[index]["imgPath"])
            if img is None: continue

            controll = log_content[index]["target"]

            # Append the new image/rotation to the current batch
            images.append(img)
            controlls.append(controll)
            # Yield the new batch
            if len(images) >= batch_size:
                yield np.array(images), np.array(controlls)
                # Next batch
                images, controlls = [], []


with open('dataSet.json') as json_file:
    content = json.load(json_file)

# Split in train/validation set
random_indexs = np.array(range(len(content)))
train_index, valid_index = train_test_split(random_indexs, test_size=0.15)

print("Training size = %s" % len(train_index))
print("Validation size = %s" % len(valid_index))

# images, rotations = next(get_data(content, train_index, 64))
#model = TmModel()
#model.compile(loss="mse", optimizer="adam")
model = build_model()
BATCH_SIZE = 40

model.fit_generator(
    generator=get_data(content, train_index, BATCH_SIZE, strict=False),
    steps_per_epoch=len(train_index) / BATCH_SIZE,
    validation_data=get_data(content, valid_index, BATCH_SIZE, strict=True),
    validation_steps=len(valid_index) / BATCH_SIZE,
    epochs=12)

# plt.imshow(images[0])
# plt.show()
model.summary()
model.save("tmModel.h5")

# 1 592 524 800
import tensorflow as tf
import cv2
from pynput import keyboard
from pynput.mouse import Button, Controller
import time
import os
import numpy as np
from PIL import ImageGrab
# from testModel import TmModel

# model = TmModel()

model = tf.keras.models.load_model('tmModel.h5')

#CONTROLL = [[0,0,0],[1,0,0],[1,1,0],[0,1,0],[1,0,1],[0,0,1],[1,1,1],[0,1,1]]
# no forward
CONTROLL = [[0,1],[1,0],[0,0]]

continu = True

def onthotControll(predict):
    global CONTROLL
    i = np.where(predict == 1)[0][0]
    controll = CONTROLL[i]
    
    # if controll[0] > 0.8:
    #     mouse.press(Button.middle)
    # else:
    #     mouse.release(Button.middle)
    if controll[0] > 1:
        mouse.press(Button.right)
    else:
        mouse.release(Button.right)
    if controll[1] > 1:
        mouse.press(Button.left)
    else:
        mouse.release(Button.left)


def controll(predict):
    # if predict[0] > 0.8:
    #     mouse.press(Button.middle)
    # else:
    #     mouse.release(Button.middle)
    if predict[0] > 0.4:
        mouse.press(Button.right)
    else:
        mouse.release(Button.right)
    if predict[1] > 0.4:
        mouse.press(Button.left)
    else:
        mouse.release(Button.left)


def on_press(key):
    global continu
    if key == keyboard.Key.esc:
        listener.stop()
        continu = False
        return False


def on_release(key):
    try:
        if key == keyboard.Key.esc:
            print('End record input')
    except KeyError:
        pass


listener = keyboard.Listener(
    on_press=on_press,
    on_release=on_release)
listener.start()
scale_percent = 50
cv2.namedWindow("input", cv2.WINDOW_NORMAL)
cv2.resizeWindow("input", 960,540)
mouse = Controller()
a = input("start ? O/N")
if a == "O":
    time.sleep(3)

    while continu:
        start = time.time()
        ss = ImageGrab.grab()
        src = cv2.cvtColor(np.array(ss), cv2.COLOR_BGR2RGB)

        width = int(src.shape[1] * scale_percent / 100)
        height = int(src.shape[0] * scale_percent / 100)
        img = cv2.resize(src, (width, height))

        cv2.imshow("input",img)
        cv2.waitKey(1) 
        img = img.reshape((-1, 540, 960, 3))
        # print(time.time() - start)
        predic = model.predict(img)[0]
        # print(time.time() - start)
        controll(predic)
        os.system('cls')
        print(predic)
        print('FPS : ', 1. / (time.time() - start))

print("end")
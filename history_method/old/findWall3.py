import cv2
import numpy as np
import time
import math
import threading
import os

# folder = "dataset/images/testScreenNew/"
folder = "dataset/images/circuit2/"
maskCar = cv2.imread('test/maskTM/mask3.png', 0)

nbFile = len(os.listdir(folder))

trakingBar = False
# l_h, l_s, l_v, u_h, u_s, u_v = 45, 0, 180, 255, 30, 255
# l_h, l_s, l_v, u_h, u_s, u_v = 0, 0, 180, 255, 255, 255
l_h, l_s, l_v, u_h, u_s, u_v = 0, 0, 180, 255, 90, 250

coefA = [10, 5, 2, 1.5, 1, 0.8, 0.6]
signes = [-1, 1]
xLine = 480
affineFunc = lambda a, x, b : round(a * x + b)

distanceTmp = [0] * (len(coefA)*len(signes))

frameI=0
pause = False
posX, posY = [], []

def posInter(img1, img2, minColor,index, interval):
    global posX ,posY
    intersection =  cv2.bitwise_and(img1, img2)
    positions = np.where(intersection >= (255-minColor, 255-minColor, 255-minColor))
    # print("--", index)
    # print(positions[1])
    # x = positions[1] + interval*index
    # print(x)
    x = positions[1].tolist()
    y = positions[0].tolist()
    posX = posX + x
    posY = posY + y

def findDistanceWall(c):
    global posX ,posY
    contour = np.zeros(orig.shape)
    cv2.drawContours(contour, [c], -1, (255, 255, 255), 1)

    colors = []
    color = 0
    lines = np.zeros(orig.shape)
    for signe in signes:
        for a in coefA:
            x = xLine*signe
            y = affineFunc(a, x, 0)*signe            
            lines = cv2.line(lines , (x+480, 540-y), (480, 530), (255-color, 255-color, 255-color), 2)
            colors.append(255-color)
            color += 1
    
    posX, posY = [], []
    interval = orig.shape[1]//3
    threads = []
    for i in range(3):
        thread = threading.Thread(target=posInter, args=(contour[i:i+interval], lines[i:i+interval], color, i, interval,))
        threads.append(thread)
        thread.start()
    
    for thread in threads:
        thread.join()

    # print("x")
    # print(posX[0])
    # print("y")
    # print(posY[0])
    # print("color")
    # print(int(lines[posY[0], posX[0]][0]))
    # print(abs(int(lines[posY[0], posX[0]][0])-255))
    # print(len(colors))


    positions = zip(posX, posY)

    result = []
    for color in colors:
        result.append([])
    
    for pos in positions:
        # print(pos[0], pos[1])
        c = int(lines[pos[1], pos[0]][0])
        # print(c)
        if c > 200:
            result[abs(c-255)].append(pos)
    
    distanceRes = []   
    for i, line in enumerate(result):
        if len(line) > 0:
            if len(line) > 1:
                line = sorted(line, key=lambda x: round(math.sqrt((480 - x[0])**2 + (540 - x[1])**2)), reverse=True)
            distanceRes.append(line[0])
    
    distanceRes = sorted(distanceRes, key=lambda x: x[0])

    return distanceRes, contour


def initWindowsBar():
    cv2.namedWindow("Tracking")
    cv2.resizeWindow("Tracking", 600,300)
    cv2.createTrackbar("LH", "Tracking", 0, 255, lambda x : None)
    cv2.createTrackbar("LS", "Tracking", 0, 255, lambda x : None)
    cv2.createTrackbar("LV", "Tracking", 0, 255, lambda x : None)
    cv2.createTrackbar("UH", "Tracking", 255, 255, lambda x : None)
    cv2.createTrackbar("US", "Tracking", 255, 255, lambda x : None)
    cv2.createTrackbar("UV", "Tracking", 255, 255, lambda x : None)

def displayWindowsBar():
    global l_h, l_s, l_v, u_h, u_s, u_v
    l_h = cv2.getTrackbarPos("LH", "Tracking")
    l_s = cv2.getTrackbarPos("LS", "Tracking")
    l_v = cv2.getTrackbarPos("LV", "Tracking")

    u_h = cv2.getTrackbarPos("UH", "Tracking")
    u_s = cv2.getTrackbarPos("US", "Tracking")
    u_v = cv2.getTrackbarPos("UV", "Tracking")

if trakingBar:
    initWindowsBar()

while True:
    ts = time.time()
    tload = time.time()
    tickmark=cv2.getTickCount()
    orig = cv2.imread(folder + 'test' + str(frameI) + '.png')
    frame = orig.copy()
    print("load : ", time.time() - tload)
    if not pause :
        if trakingBar:
            displayWindowsBar()

        tmask = time.time()
        lo = np.array([l_h, l_s, l_v])
        hi = np.array([u_h, u_s, u_v])

        image=cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        # image=cv2.blur(image, (8, 8))
        mask=cv2.inRange(image, lo, hi)
        # mask=cv2.erode(mask, None, iterations=2)
        # mask=cv2.dilate(mask, None, iterations=2)

        mask = cv2.bitwise_not(mask)
        mask=cv2.bitwise_and(mask, mask, mask=maskCar)
        mask = cv2.bitwise_not(mask)
        
        elements=cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        print("mask : ", time.time() - tmask)
        twall = time.time()
        distances= []
        if elements :
            c=max(elements, key=cv2.contourArea)
            inters, img1 = findDistanceWall(c)
            for i, inter in enumerate(inters):
                imgWithLine = cv2.line(frame , (inter[0], inter[1]), (480, 540), (0, 255, 0), 2)
                cv2.circle(imgWithLine, (inter[0], inter[1]), 2, (0,0,255), -1)
                cv2.putText(imgWithLine, str(i), (inter[0]-20, inter[1]), cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                distance = round(math.sqrt((480 - inter[0])**2 + (540 - inter[1])**2))
                cv2.putText(imgWithLine, str(distance), (inter[0]-20, inter[1]-20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                distances.append(distance)

        print("wall : ", time.time() - twall)
        tdisplay = time.time()
        fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
        cv2.putText(orig, "FPS: {:05.2f}".format(fps), (10, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)
        cv2.imshow('video', orig)
        cv2.imshow('mask', mask)
        cv2.imshow('line', imgWithLine)

    key=cv2.waitKey(1)
    if key==ord('q'):
        break
    if key==ord('p'):
        pause = pause == False
    print("display : ", time.time() - tdisplay)

    if not pause:
        frameI += 2
        if frameI > nbFile - 2:
            frameI = 0
    print("total : ", time.time() - ts)

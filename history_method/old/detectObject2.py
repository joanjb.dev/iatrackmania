import cv2
import numpy as np
import time

folder = "dataset/images/testScreen1/"

frameI=0
pause = False

def boxedDigit(img, c, name=None, infoOrig=None):
    leftmost, _ = tuple(c[c[:,:,0].argmin()][0])
    rightmost, _ = tuple(c[c[:,:,0].argmax()][0])
    _, topmost = tuple(c[c[:,:,1].argmin()][0])
    _, bottommost = tuple(c[c[:,:,1].argmax()][0])

    if name is not None:
        if infoOrig is not None:
            imgOrig, y, x = infoOrig
            digit = imgOrig[(topmost+y)-5:(bottommost+y)+5, (leftmost+x)-5:(rightmost+x)+5]
        else:
            digit = img[topmost-5:bottommost+5, leftmost-5:rightmost+5]
        cv2.imwrite("dataset/digit2/" + name + ".png",  digit)

    cv2.rectangle(img, (leftmost-5, topmost-5), (rightmost+5, bottommost+5), (255, 0, 0), 2)


while True:
    tickmark=cv2.getTickCount()
    orig = cv2.imread(folder + 'test' + str(frameI) + '.png')
    frame = orig.copy()

    speed = frame[430:495, 400:550]
    imgray = cv2.cvtColor(speed, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(imgray, 150, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    areaArray = []
    for i, c in enumerate(contours):
        area = cv2.contourArea(c)
        areaArray.append(area)
    
    sorteddata = sorted(zip(areaArray, contours), key=lambda x: x[0], reverse=True)
    xArray = []
    indexC = 0
    indexMax = 3
    while indexC < indexMax:
        drawble = True
        c = sorteddata[indexC][1]
        leftmost, _ = tuple(c[c[:,:,0].argmin()][0])
        for xC in xArray:
            if xC - 20 < leftmost < xC +20:
                indexMax += 1
                drawble = False
                break

        if drawble : 
            name = str(frameI) + "_" + str(indexC)
            # boxedDigit(speed, c, name, (orig, 430, 400))
            boxedDigit(speed, c)
            xArray.append(leftmost)
        indexC += 1
    fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
    cv2.putText(orig, "FPS: {:05.2f}".format(fps), (10, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)
    cv2.imshow('video', orig)
    cv2.imshow('speed', speed)

    key=cv2.waitKey(1)
    if key==ord('q'):
        break
    if key==ord('p'):
        pause = pause == False

    if not pause:
        frameI += 2
        if frameI > 898:
            frameI = 0
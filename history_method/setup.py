print("Start install...")

import sys

print("Checking Python version:")
if not(sys.version_info.major == 3 and sys.version_info.minor == 8):
    exit("Error, you need to install Python version 3.8.x, Your version is : " + sys.version)
print("Python version Valide, " + sys.version + "\n")

from os.path import exists, isdir
from os import mkdir
from json import dump, load
from pkg_resources import working_set
import subprocess

print("Checking package.\n")
package_installed = {pkg.key for pkg in working_set}

with open(CONFIGFILE) as json_file:
    packageConfig = load(json_file)

if packageConfig["F0"]:
    print("Package of recording data is already install.")
    active_f0 = True
else:
    active_f0 = False
    print("Package of recording data isn't install.")
    packages = [{'name': 'opencv-python', 'version': "==4.4.0.46"}, {'name' : 'pygame', 'version': '==2.0.0'}]
    packages_name = [pkg['name'] for pkg in packages]

    print("Packages to install : ", packages_name)
    print("You can to install all packages manually if you want.")
    print("If you use a software for manage your environement, remember to active it before.(for exemple : anaconda)")

    answers = input("Do you want install packages ? (y/n)")
    if answers == 'y':
        for package in packages:
            if not package['name'] in package_installed:
                subprocess.check_call([sys.executable, "-m", "pip", "install", package['name']+package['version']])
            else:
                print(package, " is already install.")
        active_f0 = True
    else:
        print("You can install manually the following packages : ")
        for pkg in packages:
            print(pkg['name']+pkg['version'])
    print("")

if packageConfig["F1"]:
    print("Package of creating dataset is already install.")
    active_f1 = True
else:
    active_f1 = False
    print("Package of recording data isn't install.")
    packages = [{'name': 'opencv-python', 'version': "==4.4.0.46"},
                {'name': 'tensorflow', 'version': "==2.3.1"}, 
                {'name': 'numpy', 'version': "==1.19.2"}]
    packages_name = [pkg['name'] for pkg in packages]

    print("Packages to install : ", packages_name)
    print("You can to install all packages manually if you want.")
    print("If you use a software for manage your environement, remember to active it before.(for exemple : anaconda)")

    answers = input("Do you want install packages ? (y/n)")
    if answers == 'y':
        for package in packages:
            if not package['name'] in package_installed:
                subprocess.check_call([sys.executable, "-m", "pip", "install", package['name']+package['version']])
            else:
                print(package, " is already install.")
        active_f1 = True
    else:
        print("You can install manually the following packages : ")
        for pkg in packages:
            print(pkg['name']+pkg['version'])
    print("")

print("For the driving script, you need to install 'vJoy' at http://vjoystick.sourceforge.net/site/ to emulate a controller on trackmania.")
print("Let setting of trackmania by default for this controller named 'vJoy Devise'.")
if packageConfig["F2"]:
    print("Package of driving car is already install.")
    active_f2 = True
else:
    active_f2 = False
    print("Package of recording data isn't install.")
    packages = [{'name': 'opencv-python', 'version': "==4.4.0.46"}, 
                {'name': 'tensorflow', 'version': "==2.3.1"},
                {'name': 'pynput', 'version': "==1.7.1"}, 
                {'name': 'numpy', 'version': "==1.19.2"}, 
                {'name': 'pyvjoy', 'version': "==1.0.1"},
                {'name': 'pillow', 'version': "==8.0.1"}]
    packages_name = [pkg['name'] for pkg in packages]

    print("Packages to install : ", packages_name)
    print("You can to install all packages manually if you want.")
    print("If you use a software for manage your environement, remember to active it before.(for exemple : anaconda)")

    answers = input("Do you want install packages ? (y/n)")
    if answers == 'y':
        for package in packages:
            if not package['name'] in package_installed:
                subprocess.check_call([sys.executable, "-m", "pip", "install", package['name']+package['version']])
            else:
                print(package, " is already install.")
        active_f2 = True
    else:
        print("You can install manually the following packages : ")
        for pkg in packages:
            print(pkg['name']+pkg['version'])
    

print("\nSaving Configuration...")
with open(CONFIGFILE, 'w') as outfile:
    dump({"F0": active_f0, "F1": active_f1, "F2": active_f2}, outfile)
print("Saving Configuration Done.\n")

print("You are ready to run launch sript !")
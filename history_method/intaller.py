from tkinter import *
from tkinter import ttk
from os.path import exists
from shutil import copyfile
from json import dump, load
from pkg_resources import working_set
import subprocess
import pathlib
import threading

class MenuPage(Frame):
    def __init__(self, parent, controller):
        Frame.__init__(self,parent)

        self.config = None
        if exists("./config/config.json"):
            with open("./config/config.json") as json_file:
                self.config = load(json_file)
            
        self.package_installed = {pkg.key for pkg in working_set}

        # création d'un widget Frame dans la fenêtre principale
        topFrame = Frame(self, borderwidth=2)
        topFrame.pack(side=TOP, padx=2, pady=2)

        Label(topFrame,text="Installeur").pack(padx=10,pady=10)

        # création d'un widget Frame dans la fenêtre principale
        selectFrame = Frame(self, borderwidth=2, relief=GROOVE)
        selectFrame.pack(side=LEFT,padx=10,pady=10)

        # création d'un second widget Frame dans la fenêtre principale
        self.displayFrame = Frame(self, borderwidth=2, relief=GROOVE)
        self.displayFrame.pack(side=LEFT, padx=10, pady=10)
        self.LbPackages = Listbox(self.displayFrame)

        self.btnInstall = Button(self, text="Install",fg='navy')
        self.progressbar = ttk.Progressbar(self, orient = HORIZONTAL, length = 100, mode = 'determinate')
        self.lInfo = Label(self)

        Label(selectFrame, text="Module available :").pack()
        self.Lb1 = Listbox(selectFrame)

        self.displayUpdate()
        self.Lb1.bind("<<ListboxSelect>>", self.selectModule)
    
    def tkraise(self):
        with open("./config/config.json") as json_file:
            self.config = load(json_file)
        self.displayUpdate()
        Frame.tkraise(self)

    def displayUpdate(self):
        self.Lb1.delete(0, END)
        if self.config:
            for index, module in enumerate(self.config["Modules"]):
                self.Lb1.insert(index, module["name"])
                bg = 'green' if module["isInstall"] else 'red'
                self.Lb1.itemconfig(index, {'bg':bg})
        self.Lb1.pack()


    def selectModule(self, event):
        if len(event.widget.curselection()) > 0:
            moduleIndex = event.widget.curselection()[0]
            self.LbPackages.delete(0, END)
            if self.config:
                module = self.config["Modules"][moduleIndex]
                for index, package in enumerate(module["packages"]):
                    self.LbPackages.insert(index, package["name"])

            self.btnInstall.config(command=lambda: self.installPackages(module))
            self.btnInstall.pack(padx=10,pady=10)
            self.LbPackages.pack()

    def installPackages(self, module):
        if module["isInstall"]:
            self.lInfo.config(text="Already install")
            self.lInfo.pack(padx=10,pady=10)
            return

        self.progressbar["value"]=0
        for i, package in enumerate(module["packages"]):
            if not package['name'] in self.package_installed:
                self.lInfo.config(text="Installation  of " + package['name'])
                subprocess.check_call([sys.executable, "-m", "pip", "install", package['name']+package['version']])
            else:
                self.lInfo.config(text="Skip installation of " + package['name'] + ",  already install")
            self.progressbar["value"]=round((i/len(module["packages"]))*100)
        self.progressbar["value"]=100
        self.lInfo.config(text="Installation done.")

        module["isInstall"] = True
        self.progressbar.pack(padx=10,pady=10)
        self.lInfo.pack(padx=10,pady=10)
        self.saveConfig()
        self.displayUpdate()
        

    def saveConfig(self):
        with open("./config/config.json", 'w') as json_file:
            dump(self.config, json_file)


class SetupPage(Frame):
    def __init__(self, parent, controller):
        Frame.__init__(self,parent)
        
        self.progressbar = ttk.Progressbar(self, orient = HORIZONTAL, length = 100, mode = 'determinate')
        self.progressbar.pack(padx=10,pady=10)

        self.info = Label(self,text="setup")
        self.info.pack(padx=10,pady=10)
        self.valid = Button(self,text="OK",fg='navy', state=DISABLED, command=lambda: controller.show_frame(MenuPage))
        self.valid.pack(padx=10,pady=10)
        
        if not exists("./config/config.json"):
            threading.Thread(target=self.setup).start()

    def setup(self):
        self.progressbar["value"]=0
        self.info.config(text="Create config by default...")
        copyfile("./config/default_config.json", "./config/config.json")
        self.progressbar["value"]=50
        self.info.config(text="Create folder dataset...")
        pathlib.Path("./dataset/drive/controller/").mkdir(parents=True, exist_ok=True)
        pathlib.Path("./dataset/drive/keyboard/").mkdir(parents=True, exist_ok=True)
        pathlib.Path("./dataset/speed/digit/").mkdir(parents=True, exist_ok=True)
        self.progressbar["value"]=100
        self.info.config(text="Done")
        self.valid.config(state=NORMAL)

        
class SeaofBTCapp(Tk):

    def __init__(self, *args, **kwargs):
        Tk.__init__(self, *args, **kwargs)
        container = Frame(self)

        container.pack(side="top", fill="both", expand = True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (SetupPage, MenuPage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        if exists("./config/config.json"):
            self.show_frame(MenuPage)
        else:
            self.show_frame(SetupPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


# Création de la fenêtre principale
mainWindow = SeaofBTCapp()
mainWindow.title('Configurateur')
mainWindow.geometry("600x300")
mainWindow.resizable(0, 0)

mainWindow.mainloop()
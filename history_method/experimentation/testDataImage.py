import cv2
import threading
from dataImage import DataImage
import time
import json
import os
import tensorflow as tf
from PIL import ImageGrab
import numpy as np

scale_percent = 50

# folder = "dataset/drive/keyboard/race1/images/"
folder = "dataset/drive/test/t2/images/"
frameI=0

pause = False

dataImg = DataImage()
model = tf.keras.models.load_model('model/tm/keyboard/tmModelDataProcess.h5')
newDS = []
nbFile = len(os.listdir(folder))
speed = 0

isThreadStart = False

meanFps = []

print("start")
start=time.time()
while True:
    ts = time.time()
    tickmark=cv2.getTickCount()
    orig = cv2.imread(folder + 'test' + str(frameI) + '.png')
    img = orig.copy()
    dataImg.setImage(orig)

    # if not isThreadStart: 
    #     dataImg.launchReadSpeed()
    #     isThreadStart = True
    
    tUpdate = time.time()
    # if (frameI % 2) == 0:
    # dataImg.updateSpeed()
    # distances, pos = dataImg.getWallDistance()
    # speed = dataImg.speed
    speed, distances, pos = dataImg.getInfos()
    # else:
        # distances = dataImg.distances

    distancesFormat = []
    speedFormat = round((speed/500), 3)

    for i, distance in enumerate(distances):
        distance = round((distance / 700), 3)
        distancesFormat.append(distance)
        cv2.circle(img, (pos[i][0], pos[i][1]), 2, (0,0,255), -1)
        cv2.putText(img, str(distance), (pos[i][0]-20, pos[i][1]-20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
        cv2.line(img , (pos[i][0], pos[i][1]), (480, 540), (0, 255, 0), 2)

    newDS.append({"wallDistances": distancesFormat, "speed": speedFormat, "target": "[0, 0, 0]"})

    print("update data: ", time.time() - tUpdate)

    inputs = distancesFormat + [speedFormat]
    inputs = tf.expand_dims(inputs, 0)

    tPredic = time.time()
    print(inputs)
    inputs_dataset = tf.data.Dataset.from_tensor_slices(inputs).batch(1)
    predic = model.predict(inputs_dataset)[0]
    print("predict: ", time.time() - tPredic)

    # ss = ImageGrab.grab()
    # orig = cv2.cvtColor(np.array(ss), cv2.COLOR_RGB2BGR)
    # src = orig.copy()

    # width = int(src.shape[1] * scale_percent / 100)
    # height = int(src.shape[0] * scale_percent / 100)
    # img = cv2.resize(src, (width, height))
    # time.sleep(0.01)

    fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
    meanFps.append(fps)
    cv2.putText(img, "FPS: {:05.2f}, Speed: {:d}, frame: {:d}".format(fps, speed, frameI), (10, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)
    cv2.imshow('video', img)
    if dataImg.mask is not None:
        cv2.imshow('mask', dataImg.mask)

    key=cv2.waitKey(1)
    if key==ord('q'):
        # dataImg.stopReaqqdSpeed()
        break
    if key==ord('p'):
        pause = pause == False

    if not pause:
        frameI += 2
        if frameI > nbFile-2:
            # break
            frameI = 0
    print("total : ", time.time() - ts)

mean = sum(meanFps)/len(meanFps)
print("fps : ", mean)

        
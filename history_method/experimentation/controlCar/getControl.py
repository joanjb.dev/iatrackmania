import time
import pygame
import threading

direction = 0.0
accel = 0
brake = 0

xboxController = None
run = True

def getControll(xboxController):
    pygame.event.pump()
    direction = xboxController.get_axis(0)
    accel = xboxController.get_button(0)
    brake = xboxController.get_button(2)
    run = not xboxController.get_button(1)
    return  [round(direction, 2), accel, brake], run

def launch():
    global xboxController, run
    pygame.init()

    for i in range(0, pygame.joystick.get_count()):
        # create an Joystick object in our list
        # joysticks.append(pygame.joystick.Joystick(i))
        if pygame.joystick.Joystick(i).get_name().find("Xbox") != -1:
            xboxController = pygame.joystick.Joystick(i)

        # initialize them all (-1 means loop forever)
        # joysticks[-1].init()
        # print a statement telling what the name of the controller is

    if xboxController is None:
        exit("Xbox controller not find.")

    print("start")
    xboxController.init()
    
    while run:
        controlls, run = getControll(xboxController)
        print(controlls, end="\r")
        time.sleep(0.01)

    pygame.quit()
    print("end")
    
    # axes = xboxController.get_numaxes()
    # buttons = xboxController.get_numbuttons()

    # for i in range(axes):
    #     axis = xboxController.get_axis(i)
    #     print("Axis {} value: {:>6.3f}".format(i, axis))

    # for i in range(buttons):
    #     button = xboxController.get_button(i)
    #     print("Button {:>2} value: {}".format(i, button))

    
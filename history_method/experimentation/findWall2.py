import cv2
import numpy as np
import time
import math
import threading
import os

def findDistanceWall(c):
    global orig

    coefA = [10, 5, 2, 1.5, 1, 0.8, 0.6]
    signes = [-1, 1]
    xLine = 480
    affineFunc = lambda a, x, b : round(a * x + b)

    distanceTmp = [0] * (len(coefA)*len(signes))

    t1 = time.time()
    contour = np.zeros((orig.shape[0], orig.shape[1]))
    print((orig.shape[0], orig.shape[1]))
    cv2.drawContours(contour, [c], -1, (255, 255, 255), 2)

    colors = []
    i = 0
    lines = np.zeros((orig.shape[0], orig.shape[1]))
    for signe in signes:
        for a in coefA:
            x = xLine*signe
            y = affineFunc(a, x, 0)*signe            
            lines = cv2.line(lines , (x+480, 540-y), (480, 530), 255-i, 1)
            colors.append(255-i)
            i += 1
    print("t1 : ", time.time() - t1)
    t2 = time.time()
    intersection =  cv2.bitwise_and(contour, lines)
    print("t2 : ", time.time() - t2)
    result = []
    for color in colors:
        result.append([])
    t3 = time.time()
    positions = np.where(intersection >= 255-i)
    positions = zip(positions[1], positions[0])
    print("t3 : ", time.time() - t3)

    for pos in positions:
        c = int(intersection[pos[1], pos[0]])
        result[abs(c-255)].append(pos)
    
    distanceRes = []   
    for i, line in enumerate(result):
        if len(line) > 0:
            if len(line) > 1:
                line = sorted(line, key=lambda x: round(math.sqrt((480 - x[0])**2 + (540 - x[1])**2)), reverse=True)
            distanceRes.append(line[0])
    
    distanceRes = sorted(distanceRes, key=lambda x: x[0])
    return distanceRes, contour, intersection


def initWindowsBar():
    cv2.namedWindow("Tracking")
    cv2.resizeWindow("Tracking", 600,300)
    cv2.createTrackbar("LH", "Tracking", 0, 255, lambda x : None)
    cv2.createTrackbar("LS", "Tracking", 0, 255, lambda x : None)
    cv2.createTrackbar("LV", "Tracking", 0, 255, lambda x : None)
    cv2.createTrackbar("UH", "Tracking", 255, 255, lambda x : None)
    cv2.createTrackbar("US", "Tracking", 255, 255, lambda x : None)
    cv2.createTrackbar("UV", "Tracking", 255, 255, lambda x : None)

def displayWindowsBar():
    global l_h, l_s, l_v, u_h, u_s, u_v
    l_h = cv2.getTrackbarPos("LH", "Tracking")
    l_s = cv2.getTrackbarPos("LS", "Tracking")
    l_v = cv2.getTrackbarPos("LV", "Tracking")

    u_h = cv2.getTrackbarPos("UH", "Tracking")
    u_s = cv2.getTrackbarPos("US", "Tracking")
    u_v = cv2.getTrackbarPos("UV", "Tracking")

def launch():
    global orig
    folder = "dataset/drive/controller/race1/images/"
    # folder = "dataset/drive/keyboard/race1/images/"

    maskCar = cv2.imread('utils/maskTM/maskCar.png', 0)

    nbFile = len(os.listdir(folder))

    imgSpecific = None
    trakingBar = False
    # l_h, l_s, l_v, u_h, u_s, u_v = 45, 0, 180, 255, 30, 255
    # l_h, l_s, l_v, u_h, u_s, u_v = 0, 0, 180, 255, 255, 255
    # l_h, l_s, l_v, u_h, u_s, u_v = 10, 0, 180, 255, 90, 250
    l_h, l_s, l_v, u_h, u_s, u_v = 10, 0, 170, 255, 80, 255

    
    frameI=0
    pause = False

    if trakingBar:
        initWindowsBar()

    while True:
        tickmark=cv2.getTickCount()
        if imgSpecific is None:
            orig = cv2.imread(folder + 'img' + str(frameI) + '.png')
        else:
            orig = cv2.imread(folder + 'test' + str(imgSpecific) + '.png')
        frame = orig.copy()
        print("--")
        ts = time.time()
        if not pause :
            if trakingBar:
                displayWindowsBar()

            tmask = time.time()
            lo = np.array([l_h, l_s, l_v])
            hi = np.array([u_h, u_s, u_v])

            image=cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
            image=cv2.blur(image, (8, 8))
            mask=cv2.inRange(image, lo, hi)
            # mask=cv2.erode(mask, None, iterations=2)
            # mask=cv2.dilate(mask, None, iterations=2)

            mask = cv2.bitwise_not(mask)
            mask=cv2.bitwise_and(mask, mask, mask=maskCar)
            mask = cv2.bitwise_not(mask)
            
            elements=cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
            print("mask : ", time.time() - tmask)
            twall = time.time()
            distances= []
            if elements :
                c=max(elements, key=cv2.contourArea)
                twall = time.time()
                inters, img1, img2 = findDistanceWall(c)
                print("wall : ", time.time() - twall)
                for i, inter in enumerate(inters):
                    imgWithLine = cv2.line(frame , (inter[0], inter[1]), (480, 540), (0, 255, 0), 2)
                    cv2.circle(imgWithLine, (inter[0], inter[1]), 2, (0,0,255), -1)
                    cv2.putText(imgWithLine, str(i), (inter[0]-20, inter[1]), cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                    distance = round(math.sqrt((480 - inter[0])**2 + (540 - inter[1])**2))
                    cv2.putText(imgWithLine, str(distance), (inter[0]-20, inter[1]-20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                    distances.append(distance)

            fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
            cv2.putText(orig, "FPS: {:05.2f}, frame: {:d}".format(fps, frameI), (10, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)
            cv2.imshow('video', orig)
            cv2.imshow('mask', mask)
            cv2.imshow('contour', img2)
            cv2.imshow('line', imgWithLine)

        
        print("total : ", time.time() - ts)

        key=cv2.waitKey(1)
        if key==ord('q'):
            break
        if key==ord('p'):
            pause = pause == False

        if not pause:
            frameI += 1
            if frameI > nbFile - 1:
                frameI = 0
        

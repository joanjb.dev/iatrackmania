import tensorflow as tf
import cv2
import numpy as np
import time
import os

def boxedDigit(img, c, infoOrig):
    leftmost, _ = tuple(c[c[:,:,0].argmin()][0])
    rightmost, _ = tuple(c[c[:,:,0].argmax()][0])
    _, topmost = tuple(c[c[:,:,1].argmin()][0])
    _, bottommost = tuple(c[c[:,:,1].argmax()][0])

    imgOrig, y, x = infoOrig
    digit = imgOrig[(topmost+y)-5:(bottommost+y)+5, (leftmost+x)-5:(rightmost+x)+5]

    return digit

def getDigits(img, orig):
    digits = []

    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(imgray, 150, 255, cv2.THRESH_BINARY)
    contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    areaArray = []
    for _, c in enumerate(contours):
        area = cv2.contourArea(c)
        areaArray.append(area)
    
    sorteddata = sorted(zip(areaArray, contours), key=lambda x: x[0], reverse=True)
    
    xArray = []
    indexC = 0
    indexMax = 3
    # print(len(sorteddata))
    while indexC < indexMax:
        # print(len(sorteddata[indexC]))
        drawble = True
        c = sorteddata[indexC][1]
        leftmost, _ = tuple(c[c[:,:,0].argmin()][0])
        for xC in xArray:
            if xC - 10 < leftmost < xC +10:
                indexMax += 1
                drawble = False
                break

        if drawble:
            digits.append(boxedDigit(img, c, (orig, 430, 400)))
            xArray.append(leftmost)
        indexC += 1
    
    sorteddata = sorted(zip(xArray, digits), key=lambda x: x[0])
    resDigits = []
    for d in sorteddata:
        resDigits.append(d[1])

    return resDigits, contours

def modifImage(img):
    digit = cv2.resize(img, (45, 32))
    img_array = tf.expand_dims(digit, 0)
    inputs_dataset = tf.data.Dataset.from_tensor_slices(img_array).batch(1)
    return inputs_dataset

def launch():

    # folder = "dataset/images/testScreen1/"
    # folder = "dataset/images/testScreenNew/"
    folder = "dataset/drive/controller/race1/images/"
    nbFile = len(os.listdir(folder))

    model = tf.keras.models.load_model('model/digits/vitesse_model3.h5')

    cv2.namedWindow('d1', cv2.WINDOW_NORMAL)
    cv2.namedWindow('d2', cv2.WINDOW_NORMAL)
    cv2.namedWindow('d3', cv2.WINDOW_NORMAL)

    cv2.resizeWindow('d1', 200,200)
    cv2.resizeWindow('d2', 200,200)
    cv2.resizeWindow('d3', 200,200)

    frameI=0
    pause = False
    while True:
        ts = time.time()
        tickmark=cv2.getTickCount()
        orig = cv2.imread(folder + 'img' + str(frameI) + '.png')
        frame = orig.copy()

        speed = frame[430:495, 400:550]

        digits, contours = getDigits(speed, orig)
        # img2 = cv2.drawContours(frame, contours, -1, 1)
        model_output1 = np.argmax(model.predict(modifImage(digits[0]))[0])
        model_output2 = np.argmax(model.predict(modifImage(digits[1]))[0])
        model_output3 = np.argmax(model.predict(modifImage(digits[2]))[0])

        speedPredict = model_output1 * 100 + model_output2 * 10 + model_output3
        print(speedPredict, frameI)
        # speedPredict = 1
        fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
        cv2.putText(orig, "FPS: {:05.2f}, speed: {:d}".format(fps, speedPredict), (10, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)
        cv2.imshow("orig", orig)
        # cv2.imshow("c", img2)
        cv2.imshow("d1", digits[0])
        cv2.imshow("d2", digits[1])
        cv2.imshow("d3", digits[2])  

        key=cv2.waitKey(1)
        if key==ord('q'):
            break
        if key==ord('p'):
            pause = pause == False

        if not pause:
            frameI += 1
            if frameI > nbFile-1:
                frameI = 0
        # print("total : ", time.time() - ts)

    # img = cv2.imread("dataset/number/3_2.png")

    # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # arrayImg = np.array(img)
    # arrayImg = np.expand_dims(arrayImg, 2)
    # lastImg = arrayImg.reshape((-1, 45, 32, 1))

    # model_output = model.predict(lastImg)

    # print(np.argmax(model_output))
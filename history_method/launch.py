from lib.dataImage import DataImage
from experimentation.testVitesse import launch
from scripts.saveDataRaw import Record
from scripts.createDS import Dataset
from scripts.driveCart import Drive

name = 'race1'
device = 'c'

def main():
    print("Welcome, Choose your scripts:")
    print("1 : Recording your race.")
    print("2 : create dataset.")
    print("3 : IA drive.")
    while True:
        try:
            answers = int(input("Enter the number."))
            break
        except:
            print("That was no valid number.  Try again...")

    if answers == 1:
        r = Record(name, device)
        r.record()
        r.saveRecord()
    if answers == 2:
        dataset = Dataset(name, device)
        dataset.processingData()
        dataset.saveDataset()
    if answers == 3:
        d = Drive("model2.h5", device, True)
        d.start()
    if answers == 7:
        launch()
        print("test")
    return

if __name__ == "__main__":
    main()
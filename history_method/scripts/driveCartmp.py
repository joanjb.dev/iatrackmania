import cv2
import time
from dataImage import DataImage
from PIL import ImageGrab
from pynput import keyboard
import pyvjoy
import numpy as np
import tensorflow as tf
import os


continu = True
scale_percent = 50
cv2.namedWindow("input", cv2.WINDOW_NORMAL)
cv2.resizeWindow("input", 960,540)

saveRace = False

DIRIMAGES = './raceSave/race2/'
imgsRace = []
if not os.path.isdir(DIRIMAGES):
    os.mkdir(DIRIMAGES)

j = pyvjoy.VJoyDevice(1)
j.set_axis(pyvjoy.HID_USAGE_X, 0x4000)
j.set_axis(pyvjoy.HID_USAGE_RZ, 0x1)
model = tf.keras.models.load_model('modelTM2_2.h5')
dataImg = DataImage()

def on_press(key):
    global continu
    if key == keyboard.Key.esc:
        listener.stop()
        continu = False
        return False
    if  key == keyboard.Key.backspace:
        j.set_axis(pyvjoy.HID_USAGE_X, 0x4000)
        j.set_axis(pyvjoy.HID_USAGE_RZ, 0x1)

def on_release(key):
    try:
        if key == keyboard.Key.esc:
            print('End record input')
    except KeyError:
        pass

def controll(predict):
    if predict[0] > 0.5:
        j.set_axis(pyvjoy.HID_USAGE_X, 0x1)
    if predict[1] > 0.5:
        j.set_axis(pyvjoy.HID_USAGE_RZ, 0x8000)
    else:
        j.set_axis(pyvjoy.HID_USAGE_RZ, 0x1)
    if predict[2] > 0.5:
        j.set_axis(pyvjoy.HID_USAGE_X, 0x8000)

    if predict[0] <= 0.5 and predict[2] <= 0.5:
        j.set_axis(pyvjoy.HID_USAGE_X, 0x4000)

a = input("start ? O/N (start 3 seconde after press O.")

listener = keyboard.Listener(
    on_press=on_press,
    on_release=on_release)
listener.start()

isThreadStart = False

if a == "O":
    time.sleep(3)

    while continu:
        tInit = time.time()
        tickmark=cv2.getTickCount()
        ss = ImageGrab.grab()
        # print("imgGrab: ", time.time() - tInit)
        # tcolor = time.time()
        orig = cv2.cvtColor(np.array(ss), cv2.COLOR_RGB2BGR)
        src = orig.copy()
        # print("init: ", time.time() - tcolor)
        # tresize = time.time()
        width = int(src.shape[1] * scale_percent / 100)
        height = int(src.shape[0] * scale_percent / 100)
        img = cv2.resize(src, (width, height))
        print("init: ", time.time() - tInit)

        tData = time.time()
        dataImg.setImage(img)

        # if not isThreadStart: 
        #     dataImg.launchReadSpeed()
        #     isThreadStart = True

        # dataImg.updateSpeed()
        # distances, pos = dataImg.getWallDistance()
        # speed = dataImg.speed

        speed, distances, pos = dataImg.getInfos()
        print("data: ", time.time() - tData)

        tFormat = time.time()
        distancesFormat = []
        speedFormat = round((speed/500), 3)

        for i, distance in enumerate(distances):
            distance = round((distance / 700), 3)
            distancesFormat.append(distance)
            cv2.circle(img, (pos[i][0], pos[i][1]), 2, (0,0,255), -1)
            cv2.putText(img, str(distance), (pos[i][0]-20, pos[i][1]-20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
            cv2.line(img , (pos[i][0], pos[i][1]), (480, 540), (0, 255, 0), 2)

        print("format: ", time.time() - tFormat)

        tPredic = time.time()
        inputs = distancesFormat + [speedFormat]
        inputs = tf.expand_dims(inputs, 0)

        inputs_dataset = tf.data.Dataset.from_tensor_slices(inputs).batch(1)
        predic = model.predict(inputs_dataset)[0]
        print("predict: ", time.time() - tPredic)
        tcnd = time.time()
        
        controll(predic)

        fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
        cv2.putText(img, "FPS: {:05.2f}, speed: {:d}".format(fps, speed), (10, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)
        if saveRace:
            imgsRace.append(img)
        cv2.imshow("input",img)
        cv2.waitKey(1)
        print("controll and displau: ", time.time() - tcnd)

        # os.system('cls')
        print(inputs, predic, fps)
    # dataImg.stopReadSpeed()

if saveRace:
    print("saving Race...")
    for i, img in enumerate(imgsRace):
        cv2.imwrite(DIRIMAGES + 'race' + str(i) + '.png',  img)
    print("end")
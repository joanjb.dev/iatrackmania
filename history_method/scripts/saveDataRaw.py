from os.path import exists, isdir
from os import mkdir
from json import dump, load
from pynput import keyboard
from time import sleep, time
from cv2 import resize, cvtColor, COLOR_BGR2RGB, imwrite
from PIL.ImageGrab import grab
import numpy as np
import pygame

class Record:
    PATH = './dataset/drive/'
    FILEDATARAW = '/dataRaw.json'
    FILECONFIG = '/config.json'
    DIRIMAGES = '/images/'

    # percent by which the image is resized
    SCALE_PERCENT = 50

    def __init__(self, name, device):
        self.keyboard = False
        self.controller = False

        if device == 'k':
            self.keyboard = True
            dirNameDevice = 'keyboard/'
        if device == 'c':
            self.controller = True
            dirNameDevice = 'controller/'

        if not (self.keyboard or self.controller):
            print("No device Found, please select a valid device.")
            return

        if not isdir(self.PATH + dirNameDevice + name):
            mkdir(self.PATH + dirNameDevice + name)

        self.fileDataRaw = self.PATH + dirNameDevice + name + self.FILEDATARAW
        if not exists(self.fileDataRaw):
            with open(self.fileDataRaw, 'w') as outfile:
                dump([], outfile)

        self.fileConfig = self.PATH + dirNameDevice + name + self.FILECONFIG
        if not exists(self.fileConfig):
            with open(self.fileConfig, 'w') as outfile:
                dump({"num": 0}, outfile)

        self.pathImages = self.PATH + dirNameDevice + name + self.DIRIMAGES
        if not isdir(self.pathImages):
            mkdir(self.pathImages)
        
        self.data = []
        self.run = True
    
    def record(self):
        print('Start Saving thr record.')
        if self.keyboard:
            self.keyboardListener()
        elif self.controller:
            self.ControllerListener()

        print('3s before starting')
        sleep(3)
        print('start record')
        while self.run:
            start = time()
            if self.keyboard:
                inputs = self.getInputKeyboard()
            elif self.controller:
                inputs = self.getInputController()

            image = self.getScreen()

            infos = {"image": image, "inputs" : inputs}
            self.data.append(infos)

            sleep(0.01)
            print('Recording : ', round(1. / (time() - start)), " Fps.", end="\r")
        print('end record')
    
    def saveRecord(self):
        with open(self.fileDataRaw) as json_file:
            dataRaw = load(json_file)

        with open(self.fileConfig) as json_file:
            config = load(json_file)

        nbImgToSave = len(self.data)
        print('Saving ' + str(nbImgToSave) + ' images.')
        i=config["num"]
        for numImg, infos in enumerate(self.data):
            imageName = self.pathImages + 'img' + str(i) + '.png'
            imwrite(imageName,  infos["image"])
            dataRaw.append({"imgFilename": imageName, "target" : infos["inputs"]})
            i += 1

            print('     ' + str(round((numImg/nbImgToSave)*100)) + '%', end="\r")
            sleep(0.01)

        print('Save images Done.')

        print('Save data raw and config.')
        with open(self.fileDataRaw, 'w') as outfile:
            dump(dataRaw, outfile)

        with open(self.fileConfig, 'w') as outfile:
            dump({"num": i}, outfile)

        print('Saving Record Done.')

    def setDevice(self, device):
        self.keyboard = device == 'k'
        self.controller = device == 'c'
        if self.keyboard or self.controller:
            print("No device Found, please select a valid device.")

    def getInputKeyboard(self):
        return [self.left, self.right, self.up, self.down]

    def getInputController(self):
        pygame.event.pump()

        direction = self.controller.get_axis(0)
        accel = self.controller.get_button(0)
        brake = self.controller.get_button(2)

        self.run = not self.controller.get_button(1)
        if not self.run :
            pygame.quit()

        return  [round(direction, 2), accel, brake]

    def getScreen(self):
        screen = grab()
        image = cvtColor(np.array(screen), COLOR_BGR2RGB)

        # #calculate the 50 percent of original dimensions
        width = int(image.shape[1] * self.SCALE_PERCENT / 100)
        height = int(image.shape[0] * self.SCALE_PERCENT / 100)

        imageScale = resize(image, (width, height))

        return imageScale

    def keyboardListener(self):
        self.up = 0
        self.right = 0
        self.left = 0
        self.down = 0

        self.listener = keyboard.Listener(
            on_press=self.on_press,
            on_release=self.on_release)
        self.listener.start()
    
    def ControllerListener(self):
        pygame.init()
        self.controller = None

        for i in range(0, pygame.joystick.get_count()):
            if pygame.joystick.Joystick(i).get_name().find("Xbox") != -1:
                self.controller = pygame.joystick.Joystick(i)
        if self.controller is None:
            exit("Xbox controller not find.")
        
        self.controller.init()

    def on_press(self, key):
        if key == keyboard.Key.up:
            self.up = 1
        if key == keyboard.Key.right:
            self.right = 1
        if key == keyboard.Key.left:
            self.left = 1
        if key == keyboard.Key.down:
            self.down = 1
        if key == keyboard.Key.esc:
            self.listener.stop()
            self.run = False
            return False


    def on_release(self, key):
        try:
            if key == keyboard.Key.up:
                self.up = 0
            if key == keyboard.Key.right:
                self.right = 0
            if key == keyboard.Key.left:
                self.left = 0
            if key == keyboard.Key.down:
                self.down = 0
        except KeyError:
            pass
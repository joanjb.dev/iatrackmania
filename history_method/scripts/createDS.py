from lib.dataImage import DataImage
from os.path import exists, isdir
from json import dump, load
from cv2 import imread
from os import mkdir
from time import sleep
import numpy as np

class Dataset:
    PATH = './dataset/drive/'
    FILEDATARAW = '/dataRaw.json'
    FILEDATASET = '/dataset.json'
    DIRIMAGES = '/images/'


    def __init__(self, name, device):
        self.keyboard = False
        self.controller = False

        if device == 'k':
            self.keyboard = True
            dirNameDevice = 'keyboard/'
        if device == 'c':
            self.controller = True
            dirNameDevice = 'controller/'

        if not isdir(self.PATH + dirNameDevice + name):
            exit("Data not found for this name and device.")

        self.fileDataset = self.PATH + dirNameDevice + name + self.FILEDATASET
        if not exists(self.fileDataset):
            with open(self.fileDataset, 'w') as json_file:
                dump([], json_file)

        self.fileDataRaw = self.PATH + dirNameDevice + name + self.FILEDATARAW
        if exists(self.fileDataRaw):
            with open(self.fileDataRaw) as json_file:
                self.dataRaw = load(json_file)
        else:
            exit("Data raw not found for this name and device.")

        self.pathImages = self.PATH + dirNameDevice + name + self.DIRIMAGES
        if not isdir(self.pathImages):
            exit("Folder images not found for this name and device.")
        
        self.dataImage = DataImage()
    
    def processingData(self):
        with open(self.fileDataset) as json_file:
            self.dataset = load(json_file)

        nbDataTotal = len(self.dataRaw)
        for numData, data in enumerate(self.dataRaw):
            imagePath = data["imgFilename"]
            image = imread(imagePath)

            self.dataImage.setImage(image)
            speed, distances, _ = self.dataImage.getInfos()

            distancesFlip = np.flip(distances, 0).tolist()
            if self.keyboard:
                targetFlip = [data["target"][1], data["target"][0], data["target"][2], data["target"][3]]
            elif self.controller:
                targetFlip = [data["target"][0]*(-1), data["target"][1], data["target"][2]]

            self.dataset.append({"wallDistances": distances, "speed": speed, "target": data["target"]})
            self.dataset.append({"wallDistances": distancesFlip, "speed": speed, "target": targetFlip})

            print('     ' + str(round((numData/nbDataTotal)*100)) + '%', end="\r")
            sleep(0.01)

    def saveDataset(self):
        print('Write json Dataset.')
        with open(self.fileDataset, 'w') as outfile:
            dump(self.dataset, outfile)
        print('Done.')

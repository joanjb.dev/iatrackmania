import cv2
import time
import json
import os
from lib.dataImage import DataImage

folder = "dataset/images/circuit2/"

FILEDATASET = 'dataset/dsFull1.json'
FILECONFIG = 'dataset/confdFull1.json'
FILENEWDATASET = 'dataset/dsmFull1.json'

frameI=0
pause = False

dataImg = DataImage()


if not os.path.exists(FILENEWDATASET):
    with open(FILENEWDATASET, 'w') as outfile:
        json.dump([], outfile)

with open(FILEDATASET) as json_file:
    dataSet = json.load(json_file)

with open(FILECONFIG) as json_file:
    config = json.load(json_file)

speed = []
nbFile=config["num"]
newDS = []

print("start")
while True:
    ts = time.time()
    tickmark=cv2.getTickCount()
    # imgPath = dataSet[frameI]["imgPath"] # ./dataset/images/circuit1/test41.png
    # orig = cv2.imread(imgPath)
    orig = cv2.imread(folder + 'test' + str(frameI) + '.png')
    

    dataImg.setImage(orig)
    if (frameI % 2) == 0:
        distances, speed, _ = dataImg.getInfos()
    else:
        distances, _, _ = dataImg.getInfos()

    distancesFormat = []
    speedFormat = round((speed/500), 3)

    for distance in distances:
        distancesFormat.append(round((distance / 700), 3))

    newDS.append({"wallDistances": distancesFormat, "speed": speedFormat, "target": dataSet[frameI]["target"]})

    # print(speed, distances)

    fps=cv2.getTickFrequency()/(cv2.getTickCount()-tickmark)
    cv2.putText(orig, "FPS: {:05.2f}".format(fps), (10, 30), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 255), 1)
    cv2.imshow('video', orig)

    key=cv2.waitKey(1)
    if key==ord('q'):
        break
    if key==ord('p'):
        pause = pause == False

    if not pause:
        frameI += 2
        if frameI > nbFile-2:
            break
            # frameI = 0
    print("total : ", time.time() - ts)

# print('write json')
# with open(FILENEWDATASET, 'w') as outfile:
#     json.dump(newDS, outfile)
# print('end write json')


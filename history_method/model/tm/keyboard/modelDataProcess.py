import tensorflow as tf
import numpy as np
from random import shuffle
import os
import json
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split

FILEDATASET = 'dataset/dsmFull1.json'

BATCH_SIZE = 64
NUM_INPUT = 15
NUM_OUTPUT = 3
EPOCH = 50

with open(FILEDATASET) as json_file:
    content = json.load(json_file)

def loadData2(content, pourcent):
    arrayTrainX = []
    arrayValX = []
    arrayTrainY = []
    arrayValY = []
    indexToVal = len(content) * pourcent
    indexArray = np.array(range(len(content)))
    for i in range(20):
        shuffle(indexArray)
        for i, iContent in enumerate(indexArray):
            data = content[iContent]
            walls = data["wallDistances"]
            speed = [data["speed"]]
            target = data["target"]
            if i < indexToVal:
                arrayTrainX.append(walls + speed)
                arrayTrainY.append(target)
            else:
                arrayValX.append(walls + speed)
                arrayValY.append(target)

    return (arrayTrainX, arrayTrainY), (arrayValX, arrayValY)



def loadData(content, pourcent):
    arrayTrainX = []
    arrayValX = []
    arrayTrainY = []
    arrayValY = []
    indexToVal = len(content) * pourcent

    for index, data in enumerate(content):
        walls = data["wallDistances"]
        speed = [data["speed"]]
        target = data["target"]
        if index < indexToVal:
            arrayTrainX.append(walls + speed)
            arrayTrainY.append(target)
        else:
            arrayValX.append(walls + speed)
            arrayValY.append(target)

    return (arrayTrainX, arrayTrainY), (arrayValX, arrayValY)


(x_train, y_train), (x_test, y_test) = loadData2(content, 0.8)
print(len(x_train), len(x_test))

train_ds=tf.data.Dataset.from_tensor_slices((x_train, y_train)).batch(BATCH_SIZE)
test_ds=tf.data.Dataset.from_tensor_slices((x_test, y_test)).batch(BATCH_SIZE)

model = tf.keras.Sequential([
    tf.keras.layers.Dense(NUM_INPUT, activation=tf.nn.relu),
    tf.keras.layers.Dense(256, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.4),
    tf.keras.layers.Dense(128, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.4),
    tf.keras.layers.Dense(64, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.4),
    tf.keras.layers.Dense(32, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.4),
    tf.keras.layers.Dense(16, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.3),
    tf.keras.layers.Dense(8, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(NUM_OUTPUT)
])

model.compile(optimizer='adam',
              loss=tf.keras.losses.MeanSquaredError(),
              metrics=['accuracy'])

history = model.fit(
  x=train_ds,
  validation_data=test_ds,
  epochs=EPOCH
)

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(EPOCH)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.show()

model.summary()

model.save("modelTM4.h5")
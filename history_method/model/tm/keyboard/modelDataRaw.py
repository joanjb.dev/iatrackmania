import json
import numpy as np
from sklearn.model_selection import train_test_split
from random import shuffle
import cv2

import tensorflow as tf


# model ----
def build_model():
    """
        Build keras model
    """
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Lambda(lambda x: (x / 127) - 1, input_shape = (540, 960, 3)))
    model.add(tf.keras.layers.Cropping2D(cropping=((245, 85), (0, 0)), input_shape = (540, 960, 3)))
    model.add(tf.keras.layers.Conv2D(8, 9, strides=(4, 4), padding="same", activation="elu"))
    model.add(tf.keras.layers.Conv2D(16, 5, strides=(2, 2), padding="same", activation="elu"))
    model.add(tf.keras.layers.Conv2D(32, 4, strides=(1, 1), padding="same", activation="elu"))
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dropout(.5))
    model.add(tf.keras.layers.Dense(1024, activation="elu"))
    model.add(tf.keras.layers.Dropout(.3))
    model.add(tf.keras.layers.Dense(2))

    #ada = optimizers.Adagrad(lr=0.001)
    model.compile(loss="mse", optimizer="adam")

    return model

def get_data(log_content, index_list, batch_size, strict=True):
        images, controlls = [], []
        while True:
            if not strict:
                shuffle(index_list)
            for index in index_list:
                # Get the Image
                img = cv2.imread(log_content[index]["imgPath"])
                if img is None: continue
                # Get the Controll
                controll = log_content[index]["target"]
                # Append the new image/controll to the current batch
                images.append(img)
                controlls.append(controll)
                # Yield the new batch
                if len(images) >= batch_size:
                    yield np.array(images), np.array(controlls), [None]
                    images, controlls = [], []
# model -----


BATCH_SIZE = 64

with open('dataset/dataSetNew.json') as json_file:
    content = json.load(json_file)

# Split in train/validation set
random_indexs = np.array(range(len(content)))
train_index, valid_index = train_test_split(random_indexs, test_size=0.20)

# print("Training size = %s" % len(train_index))
# print("Validation size = %s" % len(valid_index))

# model = TmModel()
model = build_model(

)
model.fit(x=get_data(content, train_index, BATCH_SIZE, strict=False),
    steps_per_epoch=len(train_index) / BATCH_SIZE,
    validation_data=get_data(content, valid_index, BATCH_SIZE, strict=True),
    validation_steps=len(valid_index) / BATCH_SIZE,
    epochs=20)

model.summary()
model.save("tmModel.h5")

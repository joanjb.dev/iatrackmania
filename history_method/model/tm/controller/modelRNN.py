from tensorflow.keras.layers import LSTM, Lambda, Dense, Dropout, Input
from tensorflow.keras import Sequential, losses
from tensorflow import nn, data
from json import load
import matplotlib.pyplot as plt
import numpy as np

FILEDATASET = './dataset/drive/controller/race1/dataset.json'
SAVEMODELPATH = './model/tm/controller/'
MODELFILENAME = 'rnn.h5'

BATCH_SIZE = 64
NUM_INPUT = 15
NUM_OUTPUT = 3
EPOCH = 50

def loadData(content, pourcent):
    arrayTrainX = []
    arrayValX = []
    arrayTrainY = []
    arrayValY = []
    indexToVal = len(content) * pourcent

    for index, data in enumerate(content):
        distances = data["wallDistances"]
        speed = data["speed"]
        target = data["target"]

        distancesFormat = []
        speedFormat = [round((speed/500), 3)]

        for distance in distances:
            distancesFormat.append(round((distance / 700), 3))

        if index < indexToVal:
            arrayTrainX.append(distancesFormat + speedFormat)
            arrayTrainY.append(target)
        else:
            arrayValX.append(distancesFormat + speedFormat)
            arrayValY.append(target)

    return (arrayTrainX, arrayTrainY), (arrayValX, arrayValY)

def gen_batch(inputs, targets, seq_len, batch_size, noise=0):
    # Size of each chunk
    chuck_size = (len(inputs) -1)  // batch_size
    # Numbef of sequence per chunk
    sequences_per_chunk = chuck_size // seq_len
    for s in range(0, sequences_per_chunk):
        batch_inputs = np.zeros((batch_size, seq_len, 15))
        batch_targets = np.zeros((batch_size, seq_len, 3))
        for b in range(0, batch_size):
            fr = (b*chuck_size)+(s*seq_len)
            to = fr+seq_len
            for s in range(seq_len):
                batch_inputs[b][s] = inputs[s + fr]
                batch_targets[b][s] = targets[s + fr]
            
            if noise > 0:
                noise_indices = np.random.choice(seq_len, noise)
                # batch_inputs[b][noise_indices] = np.random.randint(0, vocab_size)
            
        yield batch_inputs, batch_targets

content = []
with open(FILEDATASET) as json_file:
    content = load(json_file)

(x_train, y_train), (x_test, y_test) = loadData(content, 0.8)
print(len(x_train), len(x_test))

train_ds = data.Dataset.from_tensor_slices((x_train, y_train)).batch(BATCH_SIZE)
test_ds = data.Dataset.from_tensor_slices((x_test, y_test)).batch(BATCH_SIZE)


model = Sequential([
    LSTM(32, return_sequences=True),
    Dropout(0.3),
    Dense(NUM_OUTPUT)
])

model.compile(optimizer='adam',
              loss=losses.MeanSquaredError(),
              metrics=['accuracy'])

history = model.fit(
  x=gen_batch(x_train, y_train, 3, 10),
  validation_data=gen_batch(x_test, y_test, 3, 10),
  epochs=EPOCH
)

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(EPOCH)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.show()

model.summary()

model.save(SAVEMODELPATH + MODELFILENAME)
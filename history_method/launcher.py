from tkinter import *
from tkinter import ttk
from os.path import exists
from shutil import copyfile
from json import dump, load
import pygame
import pathlib
import threading

class MenuPage(Frame):
    def __init__(self, parent, controller):
        Frame.__init__(self,parent)

        config = None
        if exists("./config/config.json"):
            with open("./config/config.json") as json_file:
                config = load(json_file)

        # création d'un widget Frame dans la fenêtre principale
        topFrame = Frame(self, borderwidth=2)
        topFrame.pack(side=TOP, padx=10, pady=10)

        Label(topFrame,text="Launcher").pack(padx=10,pady=10)

        # création d'un second widget Frame dans la fenêtre principale
        datasetFrame = Frame(self, borderwidth=2, relief=GROOVE)
        datasetFrame.pack(side=LEFT, padx=10, pady=10)

        # création d'un widget Frame dans la fenêtre principale
        controllerFrame = Frame(self, borderwidth=2, relief=GROOVE)
        controllerFrame.pack(side=LEFT, padx=10, pady=10)

        Label(datasetFrame,text="onglet dataset").pack(padx=10,pady=10)
        Button(datasetFrame,text="Acceder",fg='navy',command=lambda: controller.show_frame(DatasetPage)).pack(padx=10,pady=10)
        
        Label(controllerFrame,text="onglet conf manette").pack(padx=10,pady=10)
        controllerConfigButton = Button(controllerFrame,text="Acceder",fg='navy',command=lambda: controller.show_frame(ConfigControllerPage))
        controllerConfigButton.pack(padx=10,pady=10)
        # if config and config["installed"]["F0"]:
        #     controllerConfigButton.config(state=NORMAL)
        # else:
        #     controllerConfigButton.config(state=DISABLED)
        #     Label(controllerFrame,text="You need to install the\n module of saving data raw.").pack(padx=10,pady=10)
        
    def tkraise(self):
        with open("./config/config.json") as json_file:
            self.config = load(json_file)
        Frame.tkraise(self)

class SetupPage(Frame):
    def __init__(self, parent, controller):
        Frame.__init__(self,parent)
        self.info = Label(self,text="You need to run the installer before and install.")
        self.info.pack(padx=10,pady=10)

class DatasetPage(Frame):
    def __init__(self, parent, controller):
        Frame.__init__(self,parent)
        Label(self,text="dataset").pack(padx=10,pady=10)
        Button(self,text="Retour",fg='navy',command=lambda: controller.show_frame(MenuPage)).pack(padx=10,pady=10)

class ConfigControllerPage(Frame):
    def __init__(self, parent, controller):
        Frame.__init__(self,parent)
        topFrame = Frame(self, borderwidth=2)
        topFrame.pack(side=TOP, padx=10, pady=10)

        Label(topFrame,text="config controller").pack(padx=10,pady=10)

        self.leftFrame = Frame(self, borderwidth=2, relief=GROOVE)
        self.leftFrame.pack(side=LEFT, padx=10, pady=10)

        self.rightFrame = Frame(self, borderwidth=2, relief=GROOVE)
        self.rightFrame.pack(side=LEFT, padx=10, pady=10)

        Label(self.leftFrame,text="select your controller").pack(padx=10)
        self.comboController = ttk.Combobox(self.leftFrame)
        self.comboController.pack(padx=10)
        Button(self.leftFrame,text="Save",fg='navy',command=lambda: controller.show_frame(MenuPage)).pack(padx=10,pady=10)
        Button(self.leftFrame,text="Retour",fg='navy',command=lambda: controller.show_frame(MenuPage)).pack(padx=10,pady=10)
    
    def display(self):
        accelFrame = Frame(self.rightFrame, borderwidth=2)
        accelFrame.pack(side=TOP, padx=10, pady=5)
        brakeFrame = Frame(self.rightFrame, borderwidth=2)
        brakeFrame.pack(side=TOP, padx=10, pady=5)
        directionFrame = Frame(self.rightFrame, borderwidth=2)
        directionFrame.pack(side=TOP, padx=10, pady=5)
        recordStopFrame = Frame(self.rightFrame, borderwidth=2)
        recordStopFrame.pack(side=TOP, padx=10, pady=5)

        self.chkAccelAnalog = BooleanVar()
        self.chkAccelAnalog.set(self.config["Controll"]["useAccelAnalog"])
        Checkbutton(accelFrame, text="Use analog", variable=self.chkAccelAnalog).pack(side=LEFT, padx=10)

        self.chkBrakeAnalog = BooleanVar()
        self.chkBrakeAnalog.set(self.config["Controll"]["useBrakeAnalog"])
        Checkbutton(brakeFrame, text="Use analog", variable=self.chkBrakeAnalog).pack(side=LEFT, padx=10)

        self.controll = [0, 0, 0, 0]

        self.controll[0] = self.config["Controll"]["accelAnalog"] if self.chkAccelAnalog.get() else self.config["Controll"]["accel"]
        self.controll[1] = self.config["Controll"]["brakeAnalog"] if self.chkBrakeAnalog.get() else self.config["Controll"]["brake"]
        self.controll[2] = self.config["Controll"]["direction"]
        self.controll[3] = self.config["Controll"]["stop"]
        
        self.accelL = Label(accelFrame,text="Accel : " + self.idBtnToString(self.controll[0], self.chkAccelAnalog.get()))
        self.accelL.pack(side=LEFT, padx=10)
        Button(accelFrame,text="Assign",fg='navy',command=lambda: self.popup(0)).pack(padx=10,pady=10)
        
        self.brakeL = Label(brakeFrame,text="Brake : " + self.idBtnToString(self.controll[1], self.chkBrakeAnalog.get()))
        self.brakeL.pack(side=LEFT, padx=10)
        Button(brakeFrame,text="Assign",fg='navy',command=lambda: self.popup(1)).pack(padx=10,pady=10)

        self.directionL = Label(directionFrame,text="Direction : " + self.idBtnToString(self.controll[2], True))
        self.directionL.pack(side=LEFT, padx=10)
        Button(directionFrame,text="Assign",fg='navy',command=lambda: self.popup(2)).pack(padx=10,pady=10)

        self.recordStopL = Label(recordStopFrame,text="Stop record : " + self.idBtnToString(self.controll[3]))
        self.recordStopL.pack(side=LEFT, padx=10)
        Button(recordStopFrame,text="Assign",fg='navy',command=lambda: self.popup(3)).pack(padx=10,pady=10)

    def update_display(self):
        self.accelL.config(text="Accel : " + self.idBtnToString(self.controll[0], self.chkAccelAnalog.get()))
        self.brakeL.config(text="Brake : " + self.idBtnToString(self.controll[1], self.chkBrakeAnalog.get()))
        self.directionL.config(text="Direction : " + self.idBtnToString(self.controll[2], True))
        self.recordStopL.config(text="Stop record : " + self.idBtnToString(self.controll[3]))

    def tkraise(self):
        if not pygame.get_init():
            pygame.init()
        controllers = []
        for i in range(0, pygame.joystick.get_count()):
            controllers.append(pygame.joystick.Joystick(i).get_name())

        with open("./config/config.json") as json_file:
            self.config = load(json_file)
        
        self.display()
        
        self.comboController.config(values=controllers)
        
        Frame.tkraise(self)
    
    def idBtnToString(self, id, analog=False):
        text = "Unkown"
        if not analog :
            if id == 0:
                text = "A"
            if id == 1:
                text = "B"
            if id == 2:
                text = "X"
            if id == 3:
                text = "Y"
            if id == 4:
                text = "Left Bumper"
            if id == 5:
                text = "Right Bumper"
            if id == 6:
                text = "Back"
            if id == 7:
                text = "Start"
            if id == 8:
                text = "L. Stick In"
            if id == 9:
                text = "R. Stick In"
            if id == 10:
                text = "Guide"
        else:
            if id == 0:
                text = "L. Stick X"
            if id == 1:
                text = "L. Stick Y"
            if id == 3:
                text = "R. Stick X"
            if id == 4:
                text = "R. Stick Y"
            if id == 2:
                text = "Left Trigger"
            if id == 5:
                text = "Right Trigger"
        return text
    
    def popup(self, controllId):
        idController = self.comboController.current()
        joy = pygame.joystick.Joystick(idController)
        fInfos = Toplevel()		  # Popup -> Toplevel()
        fInfos.title('Infos')
        Button(fInfos, text='Quitter', command=fInfos.destroy).pack(padx=10, pady=10)
        fInfos.grab_set()		  # Interaction avec fenetre jeu impossible
        threading.Thread(target=self.threadEvent, args=(fInfos, joy, controllId,)).start()
        
    def threadEvent(self, popup, joy, controllId):
        joy.init()
        run = True
        while run:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                elif event.type == pygame.JOYAXISMOTION:
                    self.controll[controllId] = event.axis
                    run = False
                elif event.type == pygame.JOYHATMOTION:
                    print(event)
                    run = False
                elif event.type == pygame.JOYBUTTONDOWN:
                    self.controll[controllId] = event.button
                    run = False
        joy.quit()
        popup.destroy()
        self.update_display()

        
class SeaofBTCapp(Tk):

    def __init__(self, *args, **kwargs):
        Tk.__init__(self, *args, **kwargs)
        container = Frame(self)

        container.pack(side="top", fill="both", expand = True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (MenuPage, ConfigControllerPage, DatasetPage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")
        if exists("./config/config.json"):
            self.show_frame(MenuPage)
        else:
            self.show_frame(SetupPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


# Création de la fenêtre principale
mainWindow = SeaofBTCapp()
mainWindow.title('Configurateur')
mainWindow.resizable(0, 0)

mainWindow.mainloop()
if pygame.get_init():
    pygame.quit()
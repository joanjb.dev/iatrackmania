import cv2
import numpy as np
import glob

PATH = "./raceSave/race1/"

imgForShape = cv2.imread(PATH + "race129.png")
height, width, layers = imgForShape.shape
size = (width,height)
fourcc = cv2.VideoWriter_fourcc(*'mp4v') 

out = cv2.VideoWriter('race1.mp4', fourcc, 15, size)

img_array = []
for filename in glob.glob(PATH + "*.png"):
    img = cv2.imread(filename)
    out.write(img)
    
out.release()
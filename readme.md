# Projet d'Intelligence Artificielle de Trackmania 2020

## Introduction

Ce projet a pour but de creer une IA capable de conduire sur trackmania 2020.

Le but finale est qu'elle puisse etre capable de finir la campagne TM. (peu importe le temps des courses)

Etape :

* Creation / optimisation du model
* Apprentissage par supervisé :
  * Recuperation et sauvegarde des données des courses (script OpenPlanet)
  * Algorithme d'entrainement
* Apprentissage par renfocement :
  * Algorithme d'evaluation
  * Algorithme d'entrainement
* Script controle de la voiture

&nbsp;

### **Avancement du projet**

* ![50%](https://progress-bar.dev/50/) : Model
* ![85%](https://progress-bar.dev/85/) : Supervisé
  * ![80%](https://progress-bar.dev/70/) : Script OpenPlanet
  * ![100%](https://progress-bar.dev/100/) : Entrainement
* ![0%](https://progress-bar.dev/0/) : Renforcement
  * ![0%](https://progress-bar.dev/0/) : Evaluation
  * ![0%](https://progress-bar.dev/0/) : Entrainement
* ![100%](https://progress-bar.dev/100/) : Controle

&nbsp;

### **En cours**

**Optimisation des recherche de block et des obstacle pour le script Openplanet.**

* Triez les tableaux par la coordonnée X :
  * En Attente d'une reponse pour la correction sur la fonction Sort d'OpenPlanet

* Recherche dichotomique.

**Modelisation des blocks pour pouvoir trouver la distance entre la voiture et les murs.**

* En recherche d'une solution.

&nbsp;

## Installation du projet

### **Pour Recorde les données de vos courses**

* installation d'OpenPlanet (lien vers OpenPlanet)

### **Pour l'entrainement du model**

* python 3.8
* Tensorflow V 2.3 (GPU or CPU)

### **Pour faire courduire l'IA**

* python 3.8
* Tensorflow V 2.3 (CPU)
* pyvjoy 1.0
* installation du logiciel vJoy + manette d'Id #1 activé

### **Arborescence**

Le dossier "history_method" sont toute les anciennes methodes que j'ai essaye. (c'est le bordel)

Le dossier "model" n'est pas enregister sur le git. il contient different structure de model.

Le dossier "dataset" n'est pas enregister sur le git (contient que des images et de json lourd). Tout nouveau fichier de data(csv) doivent etre placer dans dataset/toCheck.

Le dossier "logs" n'est pas enregister sur le git. Il contient les logs des presedant entraienement. (fonctionne avec tensorboard)

### **Fonctionnement**

Pour creer les données, placer le plugin IA dans le dossier des plugin OP, et activer le dans les parametre d'OpenPlanet. Dans les options du plugin, le premier parametres dois rester decocher, le 2eme parametre 'path' sert a changer l'endroid ou vas etre enregistré le fichier des données.

Quand vous avez assez de données, Placer tout vos fichier dans ./dataset/toCheck, puis lancer le script VerifyDataFile.py.
Ensuite il reste juste a lancer l'entrainement du model (avec model.py).

Pour faire conduire l'IA, il faut activer la premiere manette du logiciel vJoy, lancer le script drive.py qui vas lancer un serveur puis dans les options OpenPlanet cocher le 1er parametre, cela vas afficher de nouveau parametre pour ce connecter et serveur local python. Quand il est connecter l'IA prend la controle.

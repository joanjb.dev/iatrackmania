import tensorflow as tf
import pandas as pd
import datetime

FILE_DATASET = './dataset/data.csv'
SAVE_MODEL_PATH = './'
MODEL_FILENAME = 'model'

BATCH_SIZE = 64
EPOCH = 25

NB_TARGET = 3

data=pd.read_csv(FILE_DATASET)

FEATURE_LABEL = data.columns.tolist()[:-NB_TARGET]
TARGET_LABEL = data.columns.tolist()[-NB_TARGET:]

NUM_INPUT = len(FEATURE_LABEL)
NUM_OUTPUT = len(TARGET_LABEL)

def split_dataset(dataset, pourcent):
    return dataset.take(round(len(dataset) * pourcent)), dataset.skip(round(len(dataset) * pourcent))

feature = data[FEATURE_LABEL].to_numpy()
target  = data[TARGET_LABEL].to_numpy()

dataset = tf.data.Dataset.from_tensor_slices((feature, target))
train_ds, val_ds = split_dataset(dataset, 0.8)
train_ds, val_ds = train_ds.batch(BATCH_SIZE), val_ds.batch(BATCH_SIZE)

normalazer = tf.keras.layers.experimental.preprocessing.Normalization()
normalazer.adapt(feature)

model = tf.keras.Sequential([
    tf.keras.layers.InputLayer(NUM_INPUT),
    normalazer,
    tf.keras.layers.Dense(64, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.3),
    tf.keras.layers.Dense(128, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(128, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(64, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(NUM_OUTPUT)
])

model.compile(optimizer='adam', 
            loss=tf.keras.losses.MeanSquaredError(),
            metrics=['accuracy'])

log_dir = "logs/fit/" + MODEL_FILENAME + "_" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

history = model.fit(
  x=train_ds,
  validation_data=val_ds,
  epochs=EPOCH,
  callbacks=[tensorboard_callback]
)

model.summary()
model.save(SAVE_MODEL_PATH + MODEL_FILENAME + '.h5')
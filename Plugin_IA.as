#name "IA_data_drive"
#author "EuMAZing"
#category "IA"

double pi = 3.14159265359;

bool isConnected = false;
bool fail_connection = false;
bool InSetting = false;
string path = IO::FromDataFolder("");
string url = "localhost";
int port = 60777;
bool isDrive = false;
auto socket = Net::Socket();

array<string> obstableIgnore = {"Lamp", "LampB", "LampC", "RoadSign", "RoadSignB", "RoadSignC", "RoadSignWrongWay"};

void Init_Setting()
{
	Json::Value settings;
	string file_setting = IO::FromDataFolder("SettingsIA.json");
	IO::File f(file_setting);
	if(IO::FileExists(file_setting)) {
		f.Open(IO::FileMode::Read);
		settings = Json::Parse(f.ReadLine());
		path = settings["path"];
		url = settings["url"];
		port = settings["port"];
		isDrive = settings["isDrive"];
	} else {
		f.Open(IO::FileMode::Write);
		Json::Value default_settings = Json::Object();
		default_settings["path"] = path;
		default_settings["url"] = url;
		default_settings["port"] = port;
		default_settings["isDrive"] = isDrive;
		f.WriteLine(Json::Write(default_settings));
	}
	f.Close();
}

void Save_Settings(string new_path, string new_url, int new_port, bool new_isDrive)
{
	IO::File f(IO::FromDataFolder("SettingsIA.json"));
	f.Open(IO::FileMode::Write);
	Json::Value new_settings = Json::Object();
	new_settings["path"] = new_path;
	new_settings["url"] = new_url;
	new_settings["port"] = new_port;
	new_settings["isDrive"] = new_isDrive;
	f.WriteLine(Json::Write(new_settings));
	f.Close();
}

void Save_Dataset(string dataset)
{	
	string filename = path + "data.csv";
	IO::File f(filename);
	if(!IO::FileExists(filename)) {
		f.Open(IO::FileMode::Write);
		//id_block,id_next_block,orientation,distance_1,distance_2,distance_3,distance_4,obstacle_1_id,obstacle_1_u,obstacle_1_v,obstacle_1_w,obstacle_2_id,obstacle_2_u,obstacle_2_v,obstacle_2_w,obstacle_3_id,obstacle_3_u,obstacle_3_v,obstacle_3_w,is_jumping,speed,gear,direction,forward,brake
		f.WriteLine("id_block,orientation,distance_1,distance_2,distance_3,distance_4,obstacle_1_id,obstacle_1_u,obstacle_1_v,obstacle_1_w,obstacle_2_id,obstacle_2_u,obstacle_2_v,obstacle_2_w,obstacle_3_id,obstacle_3_u,obstacle_3_v,obstacle_3_w,is_jumping,speed,gear,direction,forward,brake");
		f.Close();
	}
	f.Open(IO::FileMode::Append);
	f.WriteLine(dataset);
	f.Close();
}

CGameCtnChallenge@ Get_Map()
{
	auto app = GetApp();
	auto current_playground = cast<CSmArenaClient>(app.CurrentPlayground);
	if(current_playground is null)
		return null;
		
	auto map = current_playground.Map;
	
	return map;
}

CGameTerminal@ Get_Terminal()
{
	auto currentPlayground = cast<CSmArenaClient>(GetApp().CurrentPlayground);
	if(currentPlayground is null)
		return null;

	auto players = currentPlayground.GameTerminals;
	if(players.Length <= 0)
		return null;

	auto game_terminal = cast<CGameTerminal>(players[0]);
	
	return game_terminal;
}

CSmScriptPlayer@ Get_Player(CGameTerminal@ plr_terminal)
{
	if(plr_terminal is null)
		return null;

	auto game_player = cast<CSmPlayer>(plr_terminal.GUIPlayer);
	if(game_player is null)
		return null;

	auto plr_api = cast<CSmScriptPlayer>(game_player.ScriptAPI); 

	return plr_api;
}

array<CGameCtnBlock@> Get_Blocks(CGameCtnChallenge@ map)
{
	array<CGameCtnBlock@> blocks = {};
	MwFastBuffer<CGameCtnBlock@> all_blocks = map.Blocks;

	for(int i=0; i < all_blocks.Length; i++)
		if(all_blocks[i].BlockInfo.Name != "Grass")
			blocks.InsertLast(all_blocks[i]);

	return blocks;
}

array<CGameCtnAnchoredObject@> Get_AnchoredObjects(CGameCtnChallenge@ map)
{
	array<CGameCtnAnchoredObject@> anchoredObjects = {};
	MwFastBuffer<CGameCtnAnchoredObject@> all_anchoredObjects = map.AnchoredObjects;

	for(int i=0; i < all_anchoredObjects.Length; i++){
		if(obstableIgnore.Find(all_anchoredObjects[i].ItemModel.Name) < 0){
			anchoredObjects.InsertLast(all_anchoredObjects[i]);
		}
	}

	return anchoredObjects;
}

vec3 Get_Pos_Plr(CSmScriptPlayer@ plr)
{
	vec3 plr_pos = plr.Position;
	plr_pos.x = plr_pos.x/32;
	plr_pos.y = (plr_pos.y/8)+8;
	plr_pos.z = plr_pos.z/32;
	return plr_pos;
}

CGameCtnBlock@ Find_Block(array<CGameCtnBlock@> blocks, CSmScriptPlayer@ plr)
{
	vec3 plr_pos = Get_Pos_Plr(plr);
	CGameCtnBlock@ block_find = null;

	for(int i=0; i < blocks.Length; i++){
		auto block = blocks[i];
		nat3 size_block = block.BlockInfo.VariantBaseGround.Size;
		if(block.CoordX <= plr_pos.x && plr_pos.x <= (block.CoordX + size_block.x)){
			if(block.CoordY <= plr_pos.y && plr_pos.y <= (block.CoordY + size_block.y)){
				if(block.CoordZ <= plr_pos.z && plr_pos.z <= (block.CoordZ + size_block.z)){
					return block;
				}
			}
		}
	}

	return null;
}

float Get_Area_Triangle(vec3 a, vec3 b, vec3 c)
{
	float area = (a.x*(b.z-c.z) + b.x*(c.z-a.z) + c.x*(a.z-b.z))/2;
	return Math::Abs(area);
}

array<CGameCtnAnchoredObject@> Find_Around_AnchoredObjects(array<CGameCtnAnchoredObject@> anchoredObjects, CSmScriptPlayer@ plr, int raduis, int nbDetected)
{
	vec3 direction = plr.AimDirection;

	vec3 a, b, c, d;
	a.x = (((raduis/8.0)*direction.z)+(raduis*direction.x))+plr.Position.x;
	a.z = (((-raduis/8.0)*direction.x)+(raduis*direction.z))+plr.Position.z;

	b.x = (((-raduis/8.0)*direction.z)+(raduis*direction.x))+plr.Position.x;
	b.z = (((raduis/8.0)*direction.x)+(raduis*direction.z))+plr.Position.z;

	c.x = ((-raduis/8.0)*direction.z)+plr.Position.x;
	c.z = ((raduis/8.0)*direction.x)+plr.Position.z;

	d.x = ((raduis/8.0)*direction.z)+plr.Position.x;
	d.z = ((-raduis/8.0)*direction.x)+plr.Position.z;
	

	float areaRec = Get_Distance(a, b) * Get_Distance(b, c);

	array<CGameCtnAnchoredObject@> anchoredObjects_finded = {};

	for(int i=0; i < anchoredObjects.Length; i++){
		auto anchoredObject = anchoredObjects[i];
		vec3 posObject = anchoredObject.AbsolutePositionInMap;

		float sumArea = Get_Area_Triangle(a, posObject, d) + Get_Area_Triangle(d, posObject, c) + Get_Area_Triangle(c, posObject, b) + Get_Area_Triangle(posObject, b, a);

		if(sumArea < areaRec+0.5){
			anchoredObjects_finded.InsertLast(anchoredObject);
		}
	}
	
	int nbFinded = anchoredObjects_finded.Length;
	if(nbFinded < nbDetected){
		for(int i=0; i<nbDetected-nbFinded; i++){
			anchoredObjects_finded.InsertLast(null);
		}
	} else if(nbFinded > nbDetected){
		array<double> distances = {};
		for(int i=0; i<anchoredObjects_finded.Length; i++){
			distances.InsertLast(Get_Distance(anchoredObjects[i].AbsolutePositionInMap, plr.Position));
		}
		array<CGameCtnAnchoredObject@> anchoredObjects_finded_tmp = anchoredObjects_finded;
		anchoredObjects_finded.RemoveRange(0, anchoredObjects_finded.Length);

		array<double> distancesSort = distances;
		distancesSort.SortAsc();
		for(int i=0; i<nbDetected; i++){
			for(int j=0; j<distances.Length; j++){
				if(distancesSort[i] == distances[j]){
					anchoredObjects_finded.InsertLast(anchoredObjects_finded_tmp[j]);
				}
			}
		}
	}

	return anchoredObjects_finded;
}

double Get_Distance(vec3 position1, vec3 position2)
{
	double x = Math::Pow(position2.x - position1.x, 2);
	double y = Math::Pow(position2.y - position1.y, 2);
	double z = Math::Pow(position2.z - position1.z, 2);
	double distance = Math::Sqrt(x + y + z);

	return distance;
}

array<double> Get_Info_Block_Plr(CGameCtnBlock@ block, CSmScriptPlayer@ plr)
{
	nat3 size_block = block.BlockInfo.VariantBaseGround.Size;
	double orientation = plr.AimYaw;
	vec3 anchor_point1 = vec3(block.CoordX, block.CoordY, block.CoordZ);
	vec3 anchor_point2 = anchor_point1;
	vec3 anchor_point3 = anchor_point1;
	vec3 anchor_point4 = anchor_point1;
	switch(block.BlockDir){
		case 0: // North
			anchor_point2.x += size_block.x;

			anchor_point3.x += size_block.x;
			anchor_point3.z += size_block.z;

			anchor_point4.z += size_block.z;

			if(orientation < -pi/2) {
				orientation = (orientation - (pi/2)) + pi*2;
			} else {
				orientation = orientation - (pi/2);
			}
				
			break;
		case 1: // East
			anchor_point1.x += size_block.x;

			anchor_point2.x += size_block.x;
			anchor_point2.z += size_block.z;

			anchor_point3.z += size_block.z;
			break;
		case 2: // South
			anchor_point4.x += size_block.x;

			anchor_point1.x += size_block.x;
			anchor_point1.z += size_block.z;

			anchor_point2.z += size_block.z;

			if(orientation > pi/2) {
				orientation = (orientation + (pi/2)) - pi*2;
			} else {
				orientation = orientation + (pi/2);
			}

			break;
		case 3: // West
			anchor_point3.x += size_block.x;

			anchor_point4.x += size_block.x;
			anchor_point4.z += size_block.z;

			anchor_point1.z += size_block.z;

			if(orientation <= 0)
				orientation = orientation + pi;
			else
				orientation = orientation - pi;
			break;
	}

	array<double> infos = {};
	vec3 plr_pos = Get_Pos_Plr(plr);
	infos.InsertLast(orientation);
	infos.InsertLast(Get_Distance(plr_pos, anchor_point1));
	infos.InsertLast(Get_Distance(plr_pos, anchor_point2));
	infos.InsertLast(Get_Distance(plr_pos, anchor_point3));
	infos.InsertLast(Get_Distance(plr_pos, anchor_point4));

	return infos;
}

array<double> Get_Info_Object_Plr(array<CGameCtnAnchoredObject@> aroundAnchoredObjects, CSmScriptPlayer@ plr)
{
	array<double> infos = {};
	vec3 plr_pos = plr.Position;

	for(int i=0; i < aroundAnchoredObjects.Length; i++){
		auto anchoredObject = aroundAnchoredObjects[i];
		if(anchoredObject == null){
			for( int j=0; j < 4; j++){
				infos.InsertLast(0);
			}
		} else {
			vec3 posObject = anchoredObject.AbsolutePositionInMap;
			infos.InsertLast(double(anchoredObject.ItemModel.Id.Value));
			infos.InsertLast(posObject.x - plr_pos.x);
			infos.InsertLast(posObject.y - plr_pos.y);
			infos.InsertLast(posObject.z - plr_pos.z);
		}
	}

	return infos;
}

int Is_Jumping(CSmScriptPlayer@ plr)
{
	if(plr.FlyingDuration > 0){
		return 1;
	} else {
		return 0;
	}
}

int Is_Brake(CSmScriptPlayer@ plr)
{
	if(plr.EngineCurGear > 0 && plr.InputIsBraking){
		return 1;
	} else {
		return 0;
	}
}

Json::Value Get_Json_Data(CGameCtnBlock@ block, array<CGameCtnAnchoredObject@> aroundAnchoredObjects, CSmScriptPlayer@ plr)
{
	Json::Value infos_frame = Json::Object();
	Json::Value distances = Json::Array();

	if(block is null){
		distances.Add(0); distances.Add(0); distances.Add(0); distances.Add(0);
		infos_frame["id_block"] = 0;
		infos_frame["orientation"] = 0;
	} else {
		array<double> infos_block_plr = Get_Info_Block_Plr(block, plr);
		infos_frame["id_block"] = block.DescId.Value;
		infos_frame["orientation"] = infos_block_plr[0];
		distances.Add(infos_block_plr[1]);
		distances.Add(infos_block_plr[2]);
		distances.Add(infos_block_plr[3]);
		distances.Add(infos_block_plr[4]);
	}

	Json::Value obstacles = Json::Array();
	Json::Value obstacle_data = Json::Array();

	array<double> infos_object_plr = Get_Info_Object_Plr(aroundAnchoredObjects, plr);
	for(int i=0; i<infos_object_plr.Length; i++){
		if(i%4 == 0 && i!=0){
			obstacles.Add(obstacle_data);
			obstacle_data = Json::Array();
		}
		obstacle_data.Add(infos_object_plr[i]);
		if(i == infos_object_plr.Length-1){
			obstacles.Add(obstacle_data);
			obstacle_data = Json::Array();
		}
	}
	
	infos_frame["distances"] = distances;
	infos_frame["obstacles"] = obstacles;
	infos_frame["is_jumping"] = Is_Jumping(plr);
	infos_frame["speed"] = plr.Speed*3.6;
	infos_frame["gear"] = plr.EngineCurGear;

	return infos_frame;
}

string Get_Line_CSV(CGameCtnBlock@ block, array<CGameCtnAnchoredObject@> aroundAnchoredObjects, CSmScriptPlayer@ plr)
{


	string line = "";
	string obstacles = "";

	if(block is null){
		line += "0,0,0,0,0,0,";
	} else {
		array<double> infos_block_plr = Get_Info_Block_Plr(block, plr);

		line += block.DescId.Value + ","
			+ infos_block_plr[0] + ","
			+ infos_block_plr[1] + ","
			+ infos_block_plr[2] + ","
			+ infos_block_plr[3] + ","
			+ infos_block_plr[4] + ",";
	}

	array<double> infos_object_plr = Get_Info_Object_Plr(aroundAnchoredObjects, plr);

	for(int i=0; i<infos_object_plr.Length; i++){
		obstacles += infos_object_plr[i] + ",";
	}

	line += obstacles
		+ Is_Jumping(plr) + ","
		+ plr.Speed*3.6 + ","
		+ plr.EngineCurGear + ","
		+ plr.InputSteer + ","
		+ plr.InputGasPedal + ","
		+ Is_Brake(plr) + "\n";

	return line;
}

void Main()
{
	Init_Setting();

	bool ds_saved = false;
	array<CGameCtnBlock@> blocks;
	array<CGameCtnAnchoredObject@> anchoredObjects;
	string dataset = "";
	ESGamePlaygroundUIConfig__EUISequence UISequence_Previous = ESGamePlaygroundUIConfig__EUISequence::Playing;

	while(true) {
		CGameTerminal@ plr_terminal = Get_Terminal();
		CGameCtnChallenge@ map = Get_Map();
		CSmScriptPlayer@ plr = Get_Player(plr_terminal);
		if(plr_terminal is null || map is null || plr is null || plr.Post != EPost::CarDriver) {
			dataset = "";
			blocks.RemoveRange(0, blocks.Length);
			anchoredObjects.RemoveRange(0, anchoredObjects.Length);
			if(isDrive){
				if(isConnected){
					Json::Value jsonData = Json::Object();
					jsonData["code"] = 1;
					jsonData["infos"] = "restart";

					string sendData = Json::Write(jsonData);
					socket.WriteRaw(sendData);
					sleep(50);
				}
			}
			yield();
			continue;
		}

		if(!isDrive){
			if(plr_terminal.UISequence_Current == ESGamePlaygroundUIConfig__EUISequence::Finish) {
				if(UISequence_Previous == ESGamePlaygroundUIConfig__EUISequence::Playing) {
					print("save dataset");
					Save_Dataset(dataset);
				}
				UISequence_Previous = plr_terminal.UISequence_Current;
				yield();
				continue;
			}
			UISequence_Previous = plr_terminal.UISequence_Current;
		}
		
		if(blocks.Length <= 0) {
			blocks = Get_Blocks(map);
			anchoredObjects = Get_AnchoredObjects(map);
			if(blocks is null){
				yield();
				continue;
			}
		}

		CGameCtnBlock@ block = Find_Block(blocks, plr);
		array<CGameCtnAnchoredObject@> aroundAnchoredObjects = Find_Around_AnchoredObjects(anchoredObjects, plr, 100, 3);

		if(isDrive){
			if(isConnected){
				Json::Value jsonData = Json::Object();
				jsonData["code"] = 2;
				jsonData["infos"] = Get_Json_Data(block, aroundAnchoredObjects, plr);

				socket.WriteRaw(Json::Write(jsonData));
				sleep(50);
			}
		} else {
			dataset += Get_Line_CSV(block, aroundAnchoredObjects, plr);
		}

		yield();
	}
}

void RenderMenu()
{
	if(UI::MenuItem("IA Settings", "", InSetting)) {
		InSetting = !InSetting;
	}
}

void RenderInterface()
{
	if(InSetting) {
		UI::SetNextWindowSize(600, 300);
		UI::Begin("IA Settings");
		isDrive = UI::Checkbox("save dataset / drive IA", isDrive);
		UI::Separator();

		if(isDrive) {
			UI::Text("You set the script to drive.");
			UI::NewLine();
			UI::Text("Enter the url of server IA");
			url = UI::InputText("url", url);
			UI::Text("Enter the port of server IA");
			UI::InputInt("port", port);

			if(isConnected){
				if(UI::Button("Disconnection")) {
					socket.Close();
					isConnected = false;
				}
				UI::NewLine();
				UI::Text("You are connected");
			} else {
				if(UI::Button("Connection")) {
					if(socket.Connect(url, port)){
						isConnected = true;
						fail_connection = false;
					} else {
						fail_connection = true;
					}			
				}
				UI::NewLine();
				if(fail_connection){
					UI::Text("You aren't connected, fail to connect.");
				} else {
					UI::Text("You aren't connected");
				}
				
			}
			 
		} else {
			UI::Text("You set the script to record dataset.");
			UI::NewLine();
			UI::Text("Enter your path where datasets files will save. (this dir must be create)");
			path = UI::InputText("path", path);
		}

		UI::NewLine();
		if(UI::Button("save")) {
			Save_Settings(path, url, port, isDrive);
		}
		

		UI::End();
	}
}
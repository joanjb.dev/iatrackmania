import os
import glob
import pandas as pd
import numpy as np
import sys

FOLDER_DATA = './dataset/toCheck/'
SAVE_DATA_PATH = './dataset/'
SAVE_DATA_FILENAME = 'data.csv'

NB_TARGET = 3

def validValuesTarget(data):
    for value in data[-NB_TARGET:]:
        if value != 0:
            return True
    return False

def cleanData(data):
    indexRowstoDelete = []
    toDelete = False

    data.replace(to_replace='false', value=0, inplace=True)
    data.replace(to_replace='true', value=1, inplace=True)
    data.replace(to_replace=False, value=0, inplace=True)
    data.replace(to_replace=True, value=1, inplace=True)

    for index,line in enumerate(data.values):
        if np.isnan(line[0]):
            toDelete = True
            indexRowstoDelete.append(index)
        elif (toDelete or index==0) and not validValuesTarget(line):
            toDelete = True
            indexRowstoDelete.append(index)
        else:
            toDelete = False

    data.drop(indexRowstoDelete, axis=0, inplace=True)

    return data

all_csv = glob.glob(FOLDER_DATA + "*.csv")

if len(all_csv) < 1:
    sys.exit("No data csv in folder 'toCheck'.")

dataFrames = []
for filename in all_csv:
    dataFrames.append(pd.read_csv(filename, skip_blank_lines=False))

newDataFrames = []
for dataFrame in dataFrames:
    newDataFrames.append(cleanData(dataFrame))

if os.path.isfile(SAVE_DATA_PATH + SAVE_DATA_FILENAME):
    newDataFrames.append(pd.read_csv(SAVE_DATA_PATH + SAVE_DATA_FILENAME))

newCsv = pd.concat(newDataFrames)
newCsv.to_csv(SAVE_DATA_PATH + SAVE_DATA_FILENAME, index=False, header=True)

for filename in all_csv:
    os.rename(filename, filename.replace('toCheck', 'checked'))

from tensorflow.keras.models import load_model
import tensorflow as tf
import pyvjoy
import socket
import json
import time

model = load_model("model.h5")

def restartJoy():
    global joystick
    print("restart") 
    joystick.set_axis(pyvjoy.HID_USAGE_X, 0x4000)
    joystick.set_axis(pyvjoy.HID_USAGE_RZ, 0x1)
    joystick.set_axis(pyvjoy.HID_USAGE_SL0, 0x1)
    # time.sleep(1)

def init():
    global joystick

    print("init")    
    # init joystick
    joystick = pyvjoy.VJoyDevice(1)
    restartJoy()

def controllJoystock(predict):
    global joystick

    MAX_VJOY = 32767
    MIDDLE_VJOY = MAX_VJOY//2

    dataAxeX = int(predict[0] * MIDDLE_VJOY + MIDDLE_VJOY)
    joystick.set_axis(pyvjoy.HID_USAGE_X, dataAxeX)

    if predict[1] > 0.5:
        joystick.set_axis(pyvjoy.HID_USAGE_RZ, 0x8000)
    else:
        joystick.set_axis(pyvjoy.HID_USAGE_RZ, 0x1)

    if predict[2] > 0.5:
        joystick.set_axis(pyvjoy.HID_USAGE_SL0, 0x8000)
    else:
        joystick.set_axis(pyvjoy.HID_USAGE_SL0, 0x1)

def drive(data_input):
    id_block = [data_input["id_block"]]
    orientation = [data_input["orientation"]]
    distances = data_input["distances"]
    obstacle0 = data_input["obstacles"][0]
    obstacle1 = data_input["obstacles"][1]
    obstacle2 = data_input["obstacles"][2]
    is_jumping = [data_input["is_jumping"]]
    speed = [data_input["speed"]]
    gear = [data_input["gear"]]
    

    inputs = id_block + orientation + distances + obstacle0 + obstacle1 + obstacle2 + is_jumping + speed + gear
    # print(inputs)

    inputs = tf.expand_dims(inputs, 0)
    inputs_dataset = tf.data.Dataset.from_tensor_slices(inputs).batch(1)
    predict = model.predict(inputs_dataset)[0]
    print("predict : ", predict)
    controllJoystock(predict)

# drive({"id_block":1.07374e+09,"distances":[0.750063,34.6491,24.5064,0.750063],"speed":0,"gear":1,"orientation":-1.5708})

HOST = '127.0.0.1'
PORT = 60777

init()
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    print("listing on " + HOST + ":" + str(PORT))
    conn, addr = s.accept()
    with conn:
        print('Connected by', addr)
        while True:
            msg = conn.recv(1024).decode()
            # print(msg)
            if not msg:
                break

            data = json.loads(msg)
            if data["code"] == 3:
                break
            if data["code"] == 0:
                continue
            if data["code"] == 1:
                restartJoy()
            if data["code"] == 2:
                drive(data["infos"])            

# code 0 : wait
# code 1 : start run
# code 2 : run
# code 3 : stop